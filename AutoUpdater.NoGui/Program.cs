﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutoUpdater.NoGui
{
    public class Program 
    {
        
        static void Main(string[] args)
        {
            new Updater();
            
        }
    }

    class Updater : IUpdaterGui
    {
        private static AutoResetEvent _wait = new AutoResetEvent(false);

        public Updater()
        {
            var updater = new UpdateProcess(this);
            updater.Start();
            _wait.WaitOne();
        }

        #region IUpdaterGui implementation

        public void ShowDialog(string message)
        {
        }

        public void ShowMessage(string message)
        {
        }

        public void ShowProgress(int progress, int total)
        {
            Console.WriteLine(progress);
        }

        public void Quit()
        {
            _wait.Set();
            System.Environment.Exit(0);
        }

        public bool Ask(string question)
        {
            return true;
        }

        #endregion    
    }
}
