﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using AutoUpdater;
using AutoUpdater.Logging;
using Ionic.Zip;
using System.Runtime.InteropServices;

namespace UpdateTester
{
    public partial class Form1 : Form
    {
        private Logger Log = LogManager.GetCurrentClassLogger();

        public Form1()
        {
            //SaveTestData();
            LogManager.Targets.Add(new FileTarget("updater.log"));

            UpdateMe("M1234_", Path.GetDirectoryName(Application.ExecutablePath));
            
            InitializeComponent();

            CheckUpdate();
            
        }

        private void CheckUpdate()
        {
			var ass = Assembly.GetExecutingAssembly();

            var updater = new Updater(Assembly.GetExecutingAssembly(), "http://tomi.livebox.cz/platforms/Updater/Updates/versions.xml");

//            if (updater.CheckUpdate())
//            {
//                if (MessageBox.Show("New update available, do you wish to update the application?", "LIVEBOX auto update", MessageBoxButtons.YesNo) == DialogResult.Yes)
//                {
//					var path = Path.Combine(Path.GetDirectoryName(ass.Location), "runner");
//                    updater.Update(path, false);
//                    Close();
//                }
//            }

			var updates = updater.CheckUpdate();
			if (updates != null && updates.Count > 0)
			{
				var dr = new AutoUpdater.Gui.WinForms.UpdateInfoMono(updater, ass, updates).ShowDialog();
				if (dr == DialogResult.Yes)
				{
					Close();
				}
				//var sb = new StringBuilder();
				//foreach (var version in updates) {
				//    sb.Append("------------------------------------------------\n");
				//    sb.Append("Version: " + version.Version + " (" + version.Date + ")\n");
				//    sb.Append("------------------------------------------------\n");
				//    sb.Append(version.Description + "\n\n");
				//}
				
				//if (MessageBox.Show("New update available: \n\n" + sb.ToString() + "Do you wish to update the application?", "LIVEBOX auto update", MessageBoxButtons.YesNo) == DialogResult.Yes)
				//{
				//    updater.Update(false);
				//    Close();
				//}
			}

			label1.Text = ass.GetName().Version.ToString();
        }

        void SaveTestData()
        {
            var lst = new List<VersionInfo>();
            lst.Add(new VersionInfo { Date = DateTime.Now.ToString("s"), Description = "ewrwe", Url = "wre", Version = "1.0.5", PreInstallActions = new List<string> { "act1", "ac2" }, PostInstallActions = new List<string> { "act1", "ac2" }});
            lst.Add(new VersionInfo { Date = DateTime.Now.ToString("s"), Description = "ewrwe", Url = "wre", Version = "1.0.3" });

            TextWriter tr = new StreamWriter("versions.xml");
            var sr = new XmlSerializer(typeof(List<VersionInfo>));
            sr.Serialize(tr, lst);
            tr.Close();
        }

        /// <summary>Updates the update application by renaming prefixed files</summary>
        /// <param name="updaterPrefix">Prefix of files to be renamed</param>
        /// <param name="containingFolder">Folder on the local machine where the prefixed files exist</param>
        /// <returns>Void</returns>
        public static void UpdateMe(string updaterPrefix, string containingFolder)
        {

            var dInfo = new DirectoryInfo(containingFolder);
            var updaterFiles = dInfo.GetFiles(updaterPrefix + "*");
            
            foreach (var file in updaterFiles)
            {
                string newFile = Path.Combine(containingFolder, file.Name);
                string origFile = Path.Combine(containingFolder, file.Name.Substring(updaterPrefix.Length, file.Name.Length - updaterPrefix.Length));

                if (File.Exists(origFile)) { File.Delete(origFile); }

                File.Move(newFile, origFile);
            }

        }
    }
}
