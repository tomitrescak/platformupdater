﻿using SLog;
using Gtk;
using UpdateManager.Model;

namespace UpdateManager.Controls
{
    [System.ComponentModel.ToolboxItem(true)]
    public partial class LogView : NodeView
    {
        NodeStore _logNodeStore;
        TreeViewColumn _sourceColumn;
        TreeViewColumn _logMessageColumn;
        TreeViewColumn _detailsColumn;
        ConsoleTarget _ct;
        MemoryTarget _mt;
        TreePath _firstPath = new Gtk.TreePath("0");
        TreePath _lastLogPath = new Gtk.TreePath("200");


        //		public bool VisualDisabled { get; set; }

        private static object _locker = new object();

        TextBuffer _buffer;
        TextIter _insertIter;
        string source;

        public LogView()
        {

            _logNodeStore = new NodeStore(typeof(GtkLogEventInfo));
            //LogViewer.AppendColumn ("Date", new Gtk.CellRendererText (), "text", 0);
            _sourceColumn = AppendColumn("Source", new Gtk.CellRendererText(), "text", 0);
            var logMessageCell = new Gtk.CellRendererText();
            _logMessageColumn = AppendColumn("Message", logMessageCell, "text", 1);
            _logMessageColumn.SetCellDataFunc(logMessageCell, new Gtk.TreeCellDataFunc(RenderLogMessage));
            _detailsColumn = AppendColumn("Exception", new Gtk.CellRendererText(), "text", 2);

            NodeStore = _logNodeStore;
            ShowAll();

            // prepare log control
            _ct = new ConsoleTarget();
            LogManager.Targets.Add(_ct);
            // prepare memory target
            _mt = LogManager.Targets.Find(w => w is MemoryTarget) as MemoryTarget;
            if (_mt == null)
            {
                _mt = new MemoryTarget();
            }
            _mt.Logged += (evt) =>
            {
                Gtk.Application.Invoke(delegate
                {
                    lock (_logNodeStore)
                    {
                        _logNodeStore.AddNode(new GtkLogEventInfo(evt), 0);

                        if (_logNodeStore.Data.Count > 200)
                        {
                            _logNodeStore.RemoveNode(_logNodeStore.GetNode(_lastLogPath));
                        }
                    }
                });

                
                Gtk.Application.Invoke(delegate
                {
                    ScrollToCell(_firstPath, _sourceColumn, false, 0, 0);
                });
                

            };
            LogManager.Targets.Add(_mt);
        }

        public bool ShowDetails
        {
            get { return _detailsColumn.Visible; }
            set { _detailsColumn.Visible = value; }
        }

        private void RenderLogMessage(Gtk.TreeViewColumn column, Gtk.CellRenderer cell, Gtk.TreeModel model, Gtk.TreeIter iter)
        {
            var log = model.GetValue(iter, 3);

            if (log.Equals(LogLevel.Error))
            {
                (cell as Gtk.CellRendererText).Foreground = "red";
            }
            else if (log.Equals(LogLevel.Info))
            {
                (cell as Gtk.CellRendererText).Foreground = "blue";
            }
            else if (log.Equals(LogLevel.Warn))
            {
                (cell as Gtk.CellRendererText).Foreground = "orange";
            }
            else if (log.Equals(LogLevel.Fatal))
            {
                (cell as Gtk.CellRendererText).Foreground = "violet";
            }
            else
            {
                (cell as Gtk.CellRendererText).Foreground = "black";
            }
            (cell as Gtk.CellRendererText).Text = model.GetValue(iter, 1).ToString();
        }

    }
}

