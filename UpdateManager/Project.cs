using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace UpdateManager
{
	[XmlRoot]
	public class Project
	{
		[XmlElement]
		public string Name { get; set; }
		[XmlElement]
		public string Repository { get; set; }
		[XmlElement]
		public string Path { get; set; }
		[XmlElement]
		public string BinariesRoot { get; set; }
		[XmlElement]
		public string VersionFile { get; set; }
		[XmlArray("Ignores")]
		[XmlArrayItem("Ignore")]
        public List<string> Ignores { get; set; }
	}
}

