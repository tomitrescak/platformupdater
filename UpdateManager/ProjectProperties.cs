using System;
using System.Collections.Generic;
using Gtk;
using UpdateManager.Model;

namespace UpdateManager
{
	public partial class ProjectProperties : Gtk.Window
	{
		public event System.Action Saved;
		CellRendererText nameCell = new CellRendererText() { Editable = false };
		NodeStore _ignoreStore;
		Project _project;

		public ProjectProperties () : 
			base(Gtk.WindowType.Toplevel)
		{
			this.Build ();
		}

		public ProjectProperties(string title, Project project) 
			: base(Gtk.WindowType.Toplevel) {

			this.Build ();

			// init values

			ignores.AppendColumn("FileName", nameCell, "text", 0);

			projectName.Text = project.Name;
			projectRepository.Text = project.Repository;
			projectVersionFile.Text = project.VersionFile;
            binariesRoot.Text = project.BinariesRoot;
		    
			_project = project;
			
            _ignoreStore = new NodeStore(typeof(GenericNode));
            InitIgnores ();
			ignores.NodeStore = _ignoreStore;

		    this.Title = title;
		}

		private void InitIgnores() {
			_ignoreStore.Clear();
			if (_project.Ignores != null)
			{
                _project.Ignores.Sort();
				foreach (var ig in _project.Ignores) {
                    if (string.IsNullOrEmpty(ig)) continue;
					_ignoreStore.AddNode (new GenericNode (ig));
				}
			}
			ignores.ShowAll ();
            ignores.QueueDraw();
		}

		protected void SaveAction (object sender, EventArgs e)
		{
			MainWindow.CurrentProject.Name = projectName.Text;
			MainWindow.CurrentProject.Repository = projectRepository.Text;
			MainWindow.CurrentProject.VersionFile = projectVersionFile.Text;
			MainWindow.CurrentProject.BinariesRoot = binariesRoot.Text;
            MainWindow.CurrentProject.Ignores = new List<string>();
			// MainWindow.CurrentProject.Ignores.AddRange(ignores.Text.Split (';'));

			if (Saved != null) {
				Saved();
			}

            this.Destroy();
		}

		protected void Cancel (object sender, EventArgs e)
		{
			this.Destroy ();
		}

		protected void FindBinaries (object sender, EventArgs e)
		{
            var fc = new Gtk.FileChooserDialog("Choose the binaries directory",
                                                                  this,
                                                                  FileChooserAction.SelectFolder,
                                                                  "Cancel", ResponseType.Cancel,
                                                                  "Open", ResponseType.Accept);
            if (fc.Run() == (int)ResponseType.Accept)
            {
                binariesRoot.Text = fc.Filename.Replace(MainWindow.Instance.LocalProjectDirectory, "");
            }
            
            fc.Destroy();
		}

		protected void FindVersionsFile (object sender, EventArgs e)
		{
            var fc = new Gtk.FileChooserDialog("Choose the versions file",
                                                                  this,
                                                                  FileChooserAction.Open,
                                                                  "Cancel", ResponseType.Cancel,
                                                                  "Open", ResponseType.Accept);
            if (fc.Run() == (int)ResponseType.Accept)
            {
                projectVersionFile.Text = System.IO.Path.GetFileName(fc.Filename);
            }

            fc.Destroy();
		}
	
		protected void RemoveIgnore (object sender, EventArgs e)
		{
			if (ignores.NodeSelection.SelectedNode == null) {
				return;
			}

			// find local version of this project
			_project.Ignores.Remove(((GenericNode) ignores.NodeSelection.SelectedNode).Name);		
			InitIgnores ();
		}
	}
}

