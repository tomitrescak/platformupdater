using System;

namespace UpdateManager
{
	public partial class PlatformProperties : Gtk.Window
	{
		public event System.Action Saved;

		public PlatformProperties () : 
				base(Gtk.WindowType.Toplevel)
		{
			this.Build ();
		}

		public PlatformProperties(string title, Platform platform) 
			: base(Gtk.WindowType.Toplevel) {

			this.Build ();

			// init values

			ftpUrl.Text = platform.FtpUrl;
			ftpUser.Text = platform.FtpUserName;
			ftpPassword.Text = platform.FtpPassword;
			ftpRoot.Text = platform.FtpDir;
			http.Text = platform.HttpRoot;

			this.Title = title;
		}

		protected void Save (object sender, EventArgs e)
		{
			MainWindow.CurrentPlatform.HttpRoot = http.Text;
			MainWindow.CurrentPlatform.FtpUrl = ftpUrl.Text;
			MainWindow.CurrentPlatform.FtpUserName = ftpUser.Text;
			MainWindow.CurrentPlatform.FtpPassword = ftpPassword.Text;
			MainWindow.CurrentPlatform.FtpDir = ftpRoot.Text;
			MainWindow.CurrentPlatform.Save();

			if (Saved != null) {
				Saved();
			}

            this.Destroy();
		}

		protected void Cancel (object sender, EventArgs e)
		{
			this.Destroy ();
		}
	}
}

