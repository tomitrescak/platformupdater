﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Gtk;

namespace UpdateManager.Model
{
    [Gtk.TreeNode(ListOnly = true)]
    public class FileDetailNode : TreeNode
    {
        private bool _selected;
		private bool _isNewer;

        [Gtk.TreeNodeValue(Column = 0)]
        public bool Selected 
        {    get { return _selected; }
            set { _selected = value; }
        }
        [Gtk.TreeNodeValue(Column = 1)]
        public string Name { get { return _name; } }
        [Gtk.TreeNodeValue(Column = 2)]
        public string Modified { get { return _modified; } }
        [Gtk.TreeNodeValue(Column = 3)]
        public bool IsDirectory { get { return _isDirectory; } }
		[Gtk.TreeNodeValue(Column = 4)]
		public bool IsNewer { get {return _isNewer; }}

        private string _name;
        private string _modified;
        private bool _isDirectory;

        public FileDetailNode(FileInfo fi, DateTime lastDate)
        {
            _name = fi.Name;
            _modified = fi.LastWriteTime.ToString();
            _isDirectory = false;
			_isNewer = lastDate < fi.LastWriteTime;
        }

		public FileDetailNode(DirectoryInfo fi, DateTime lastDate)
        {
            _name = fi.Name;
            _modified = fi.LastWriteTime.ToString();
            _isDirectory = true;
			_isNewer = lastDate < fi.LastWriteTime;
        }
    }
}
