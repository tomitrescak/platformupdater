﻿using SLog;

namespace UpdateManager.Model
{
    [Gtk.TreeNode(ListOnly = true)]
    public class GtkLogEventInfo : Gtk.TreeNode
    {

        [Gtk.TreeNodeValue(Column = 0)]
        public string Source { get { return Event.Source.ToString(); } }
        [Gtk.TreeNodeValue(Column = 1)]
        public string Message { get { return Event.FormattedMessage; } }
        [Gtk.TreeNodeValue(Column = 2)]
        public string Exception { get { return Event.StackTrace; } }
        [Gtk.TreeNodeValue(Column = 3)]
        public LogLevel Level { get { return Event.Level; } }

        public LogEventInfo Event { get; private set; }

        public GtkLogEventInfo() { }

        public GtkLogEventInfo(LogEventInfo eventInfo)
        {
            Event = eventInfo;
        }
    }
}

