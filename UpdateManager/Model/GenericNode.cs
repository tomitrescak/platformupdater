﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UpdateManager.Model
{

    [Gtk.TreeNode(ListOnly = true)]
    public class GenericNode : Gtk.TreeNode
    {
        [Gtk.TreeNodeValue(Column = 0)]
        public string Name { get; set; }

        public GenericNode(string name)
        {
            Name = name;
        }
    }
    
}
