using System;
using System.IO;
using System.Threading;
using EnterpriseDT.Net.Ftp;
using AutoUpdater;

namespace UpdateManager
{
	public class SimpleFileInfoNode : Gtk.TreeNode
	{
		private static readonly SLog.Logger Log = SLog.LogManager.GetCurrentClassLogger("FileActions");

		private string _name;
		private string _modified;
		private string _version;
		private string _versionInfo;
		private string _root;
		private string _ftpPath;
		private bool _isDirectory;
		private FTPConnection _ftp;

		private string LocalPath { get { return Path.Combine(_root, _name); }}

		[Gtk.TreeNodeValue(Column = 0)]
		public bool Downloaded 
		{   get { return _isDirectory ? Directory.Exists(LocalPath) : File.Exists(LocalPath); }
		}
		[Gtk.TreeNodeValue(Column = 1)]
		public string Name { get { return _name; } }
		[Gtk.TreeNodeValue(Column = 2)]
		public string Modified { get { return _modified; } }
		[Gtk.TreeNodeValue(Column = 3)]
		public string Version { get { return _version; } }
		[Gtk.TreeNodeValue(Column = 4)]
		public string VersionInfo { get { return _versionInfo; } }
		
		public SimpleFileInfoNode(bool directory, string localRoot, string ftpRoot, FTPConnection ftp, string name, string modified = null, string version = null, string versionInfo = null)
		{
			_isDirectory = directory;
			_root = localRoot;
			_name = name;
			_version = version;
			_versionInfo = versionInfo;
			_ftpPath = ftpRoot + "/" + _name;
			_ftp = ftp;
		}

		public SimpleFileInfoNode(VersionInfo version, string localRoot, FTPConnection ftp)
		{
			_isDirectory = false;
			_root = localRoot;
			_name = Path.GetFileName(version.Url);
			_modified = version.Date;
			_version = version.Version;
			_versionInfo = version.Description;
			_ftpPath = version.Url;
			_ftp = ftp;
		}

		public void Toggle() {
			if (Downloaded) {
				Delete();
			} else {
				Download();
			}
		}

		private void Download() {
			if (_isDirectory) {
				new Thread(DownloadDir).Start();
			} else { 
				new Thread(DownloadFile).Start();
			}
		}

		private void Delete() {
			try {
				if (_isDirectory) {
					Directory.Delete(LocalPath, true);
				} else { 
					File.Delete(LocalPath);
				}
				Log.Info("'{0}' deleted.", _name);
			} catch (Exception ex) {
				Log.ErrorException(ex, "Error processing file or directory");
			}
		}

		// private methods

		private void DownloadFile() {
			Log.Info("Downloading '{0}' to '{1}'", _name, LocalPath);

			_ftp.ChangeWorkingDirectory(_root);
			_ftp.DownloadFile(LocalPath, _name);

			Log.Info("'{0}' downloaded.", _name);
		}

		private void DownloadDir() {
			Log.Info("Downloading '{0}' to '{1}'", _name, LocalPath);
			DownloadDir(_ftpPath, LocalPath);
			Log.Info("'{0}' downloaded.", _name);
		}

		private void DownloadDir(string remotePath, string localPath) {
			Directory.CreateDirectory(localPath);

			_ftp.ChangeWorkingDirectory(remotePath);

			var list = _ftp.GetFileInfos();

			foreach (var file in list) {
				if (!file.Dir) {
					Log.Info("Getting file '{0}'", file.Name);
					_ftp.DownloadFile(Path.Combine(localPath, file.Name), file.Name);
				} else {
					// recursively download all files
					Log.Info("Getting directory '{0}'", file.Name);
					DownloadDir(file.Path, Path.Combine(localPath, file.Name));
				}
			}
		}
	}
}

