using System;
using System.Collections.Generic;

namespace UpdateManager
{
	public static class Catalog
	{
		public static string TabGeneral = "<b>General</b>";
		public static string Save = "Save";
		public static string Cancel = "Cancel";
		public static string Open = "Open";
		public static string Exit = "Exit";

		public static string ProgramDirectory = "Applications Directory: ";
		public static string SourcesDirectory = "Sources Directory: ";

		public static string Create = "Create";
		public static string Update = "Update";
		public static string Package = "Package";

		public static string File = "File";
		public static string LocalVersion = "Local Version: ";
		public static string ServerVersion = "Server Version: ";
		public static string Settings = "Settings";

		public static string General = "General: ";
		public static string Ftp = "Ftp: ";
		public static string Url = "Ftp: ";
		public static string User = "User: ";
		public static string Password = "Password: ";
		public static string Directory = "Directory: ";
		public static string Sources = "Sources";
		public static string Library = "Library";
		public static string Packages = "Packages";
		public static string Updates = "Updates";

		public static string Include = "Include";
		public static string Name = "Name";
		public static string Date = "Date";
		public static string DownloadedAbbr = "Down.";
		public static string Version = "Version";
		public static string VersionInfo = "Version Info";
		public static string ToggleAll = "Select All";
		public static string VersionInfoText = "<Version Info>";
		public static string Refresh = "Refresh";
		public static string PreCommands = "Pre-Update Commands";
		public static string PostCommands = "Post-Update Commands";

	}
}

