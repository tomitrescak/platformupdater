using System;
using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;

namespace UpdateManager
{
	[XmlRoot]
	public class Platform
	{
		SLog.Logger Log = SLog.LogManager.GetCurrentClassLogger();

		[XmlElement]
		public string HttpRoot { get; set; }
		[XmlElement]
		public string FtpUrl { get; set; }
		[XmlElement]
		public string FtpDir { get; set; }
		[XmlElement]
		public string FtpUserName { get; set; }
		[XmlElement]
		public string FtpPassword { get; set; }
		[XmlIgnore]
		public string Path { get; set; } 
		[XmlArray]
		public List<Project> Projects { get; private set; }

		public Platform() {
			Projects = new List<Project>();
		}

		public static Platform Load(string path) {
			Platform platform;
			using (var fs = new FileStream(path, FileMode.Open))
			{
				var sr = new XmlSerializer(typeof (Platform));
				platform = (Platform) sr.Deserialize(fs);
				fs.Close();
			}
			platform.Path = path;

			return platform;
		}

		public void Save() {
			try {
				TextWriter tr = new StreamWriter(Path);
				var sr = new XmlSerializer(typeof(Platform));
				sr.Serialize(tr, this);
				tr.Close();
			} catch (Exception ex) {
				Log.ErrorException (ex, ex.Message);
			}
		}
	}
}

