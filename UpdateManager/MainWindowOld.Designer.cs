﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Gtk;
//using UpdateManager.Controls;
//
//namespace UpdateManager
//{
//    public partial class MainWindowOld
//    {
//		MenuBar mb = new MenuBar();
//		Menu filemenu = new Menu();
//		MenuItem file = new MenuItem(Catalog.File);
//		MenuItem openPlatform = new MenuItem(Catalog.Open);
//		MenuItem savePlatform = new MenuItem(Catalog.Save);
//		MenuItem settings = new MenuItem(Catalog.Settings);
//		MenuItem exit = new MenuItem(Catalog.Exit);
//
//		VBox appBox = new VBox();
//        VPaned logSplitter = new VPaned();
//        HPaned navigatorSplitter = new HPaned();
//        NodeView platformView = new NodeView();
//        LogView logView = new LogView();
//        VBox detailBox = new VBox();
//        //Label localVersion = new Label(Catalog.LocalVersion);
//        //Label serverVersion = new Label(Catalog.ServerVersion);
//        NodeView projectView = new NodeView();
//        HBox buttonBox = new HBox();
//        Button createUpdate = new Button(Catalog.Create);
//        
//		HPaned contentSplitter = new HPaned();
//		VPaned libraryUpdatesPackages = new VPaned();
//        
//		Alignment platformViewAlign = new Alignment(0.5f, 0.5f, 1, 1);
//		Alignment contentAlign = new Alignment(0.5f, 0.5f, 1, 1);
//
//		CellRendererToggle packetToggle = new CellRendererToggle() { Activatable = true };
//		CellRendererToggle downloadLibraryToggle = new CellRendererToggle() { Activatable = true };
//		CellRendererToggle downloadPackageToggle = new CellRendererToggle() { Activatable = true };
//		CellRendererToggle downloadUpdateToggle = new CellRendererToggle() { Activatable = true };
//
//		CellRendererText nameCell = new CellRendererText() { Editable = false };
//		CellRendererText colorCell = new CellRendererText() { Editable = false };
//
//		Frame sourcesFrame = new Frame(Catalog.Sources);
//		Frame libraryFrame = new Frame(Catalog.Library);
//		Frame packagesFrame = new Frame(Catalog.Packages);
//		Frame updatesFrame = new Frame(Catalog.Updates);
//
//		VBox packagesUpdatesBox = new VBox();
//		ComboBox packageUpdateSelector = ComboBox.NewText();
//
//		NodeView libraryView = new NodeView();
//		NodeView packagesView = new NodeView();
//		NodeView updatesView = new NodeView();
//
//		ScrolledWindow logScroll = new ScrolledWindow();
//		ScrolledWindow projectScroll = new ScrolledWindow();
//
//		CheckButton toggleAll = new CheckButton(Catalog.ToggleAll);
//
//		Button refreshProject = new Button(Catalog.Refresh);
//
//		Notebook parameters = new Notebook();
//		TextView versionInfo = new TextView();
//		TextView preParameters = new TextView();
//		TextView postParameters = new TextView();
//
//
//        void Initialize()
//        {
//			// init menu
//
//			openPlatform.Activated += OpenPlatform;
//			savePlatform.Activated += SavePlatform;
//			exit.Activated += ExitApplication;
//			settings.Activated += ShowSettings;
//
//			filemenu.Append(openPlatform);
//			filemenu.Append(savePlatform);
//			filemenu.Append(new SeparatorMenuItem());
//			filemenu.Append(settings);
//			filemenu.Append(new SeparatorMenuItem());
//			filemenu.Append(exit);
//			file.Submenu = filemenu;
//			mb.Append(file);
//
//			// init sources
//
//            //localVersion.SetAlignment(0, 0.5f);
//            //serverVersion.SetAlignment(0, 0.5f);
//            
//			packetToggle.Toggled += FileIncludeToggled;
//			downloadLibraryToggle.Toggled += DownloadLibraryFile;
//			downloadPackageToggle.Toggled += DownloadPackage;
//			downloadUpdateToggle.Toggled += DownloadUpdate;
//
//			platformView.AppendColumn(Catalog.Name, new Gtk.CellRendererText(), "text", 0);
//
//			projectView.AppendColumn(Catalog.Include, packetToggle, "active", 0);
//			var nameColumn = projectView.AppendColumn(Catalog.Name, colorCell, "text", 1);
//			projectView.AppendColumn(Catalog.Date, nameCell, "text", 2);
//			nameColumn.SetCellDataFunc(colorCell, new Gtk.TreeCellDataFunc(RenderFileName));
//
//			libraryView.AppendColumn(Catalog.DownloadedAbbr, downloadLibraryToggle, "active", 0);
//			libraryView.AppendColumn(Catalog.Name, nameCell, "text", 1);
//
//			packagesView.AppendColumn(Catalog.DownloadedAbbr, downloadPackageToggle, "active", 0);
//			packagesView.AppendColumn(Catalog.Name, nameCell, "text", 1);
//			packagesView.AppendColumn(Catalog.Date, nameCell, "text", 2);
//			packagesView.AppendColumn(Catalog.Version, nameCell, "text", 3);
//			packagesView.AppendColumn(Catalog.VersionInfo, nameCell, "text", 4);
//
//			updatesView.AppendColumn(Catalog.DownloadedAbbr, downloadUpdateToggle, "active", 0);
//			updatesView.AppendColumn(Catalog.Name, nameCell, "text", 1);
//			updatesView.AppendColumn(Catalog.Date, nameCell, "text", 2);
//			updatesView.AppendColumn(Catalog.Version, nameCell, "text", 3);
//			updatesView.AppendColumn(Catalog.VersionInfo, nameCell, "text", 4);
//
//			packageUpdateSelector.AppendText(Catalog.Update);
//			packageUpdateSelector.AppendText(Catalog.Package);
//			packageUpdateSelector.ShowAll();
//
//			createUpdate.Clicked += CreatePackageUpdate;
//			refreshProject.Clicked += RefreshProject;
//			toggleAll.Toggled += ToggleProjectFiles;
//
//			buttonBox.PackEnd(createUpdate, false, false, 3);
//			buttonBox.PackEnd(packageUpdateSelector, false, false, 3);
//            buttonBox.PackStart(toggleAll, false, false, 3);
//			buttonBox.PackStart(refreshProject, false, false, 3);
//
//			projectScroll.Add(projectView);
//
//			parameters.HeightRequest = 85;
//			parameters.BorderWidth = 6;
//
//			versionInfo.Buffer.Text = Catalog.VersionInfoText;
//
//			parameters.AppendPage(versionInfo, new Label(Catalog.VersionInfo));
//			parameters.AppendPage(preParameters, new Label(Catalog.PreCommands));
//			parameters.AppendPage(postParameters, new Label(Catalog.PostCommands));
//
//			//detailBox.PackStart(localVersion, false, false, 6);
//			//detailBox.PackStart(serverVersion, false, false, 6);
//			detailBox.PackStart(projectScroll, true, true, 0);
//			detailBox.PackStart(parameters, false, false, 0);
//			detailBox.PackStart(buttonBox, false, false, 0);
//
//			// init project view
//
//			platformView.NodeSelection.Changed += LoadProject;
//
//			Gdk.Color col = new Gdk.Color(229,236,238);
//			//Gdk.Color.Parse("red", ref col);
//			platformView.ModifyBase(StateType.Normal, col);
//
//			platformViewAlign.SetPadding(0,0,0,0);
//			platformViewAlign.Add(platformView);
//
//            // init main layout
//            
//			packagesFrame.Add(packagesView);
//			libraryFrame.Add(libraryView);
//			updatesFrame.Add(updatesView);
//
//			packagesUpdatesBox.PackStart(packagesFrame);
//			packagesUpdatesBox.PackStart(updatesFrame);
//
//			libraryUpdatesPackages.Add1(libraryFrame);
//			libraryUpdatesPackages.Add2(packagesUpdatesBox);
//			libraryUpdatesPackages.Position = 80;
//
//			sourcesFrame.Add(detailBox);
//
//			logScroll.Add(logView);
//
//			contentSplitter.Add1(sourcesFrame);
//			contentSplitter.Add2(libraryUpdatesPackages);
//
//			contentAlign.SetPadding(0,0,0,6);
//			contentAlign.Add(contentSplitter);
//
//			navigatorSplitter.Add1(platformViewAlign);
//			navigatorSplitter.Add2(contentAlign);
//			navigatorSplitter.Position = 120;
//
//			logSplitter.Add1(navigatorSplitter);           
//			logSplitter.Add2(logScroll);
//			logSplitter.Position = 450;
//
//			appBox.PackStart(mb, false, false, 0);
//			appBox.PackStart(logSplitter, true, true, 6);
//
//			Add(appBox);
//
//			logView.ShowAll();
//
//			Log.Info("Initialized!");
//        }
//
//        
//
//		private void RenderFileName(Gtk.TreeViewColumn column, Gtk.CellRenderer cell, Gtk.TreeModel model, Gtk.TreeIter iter)
//		{
//			var log = model.GetValue(iter, 4);
//			
//			if (log.Equals(true))
//			{
//				(cell as Gtk.CellRendererText).Foreground = "red";
//			}
//			else 
//			{
//				(cell as Gtk.CellRendererText).Foreground = "black";
//			}
//		}
//
//        
//    }
//}
