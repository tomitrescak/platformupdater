using System;
using System.Xml.Serialization;
using System.IO;

namespace UpdateManager
{
	public static class AppSettings
	{
		private static readonly SLog.Logger Log = SLog.LogManager.GetCurrentClassLogger ();

		#region Settings
		public class Settings
		{
			public string LastPlatform { get; set; }
		}
		#endregion

		// fields and constants

		const string configFile = "App.config";

		// properties

		public static Settings Default { get; private set; }

		// ctor

		#region AppSettings ()
		static AppSettings ()
		{
			// init default settings
			if (!File.Exists (configFile)) {
				Default = new Settings ();
				return;
			}


			try {
				// load sotred settings
				Settings settings;
				using (var fs = new FileStream(configFile, FileMode.Open)) {
					var sr = new XmlSerializer (typeof(Settings));
					settings = (Settings)sr.Deserialize (fs);
					fs.Close ();
				}

				Default = settings;
			} catch (Exception ex) {
				Log.ErrorException(ex, "Problem loading app settings!");
				Default = new Settings();
			}
		}
		#endregion
		// public methods

		#region Save()
		public static void Save()
		{
			TextWriter tr = new StreamWriter (configFile);
			var sr = new XmlSerializer (typeof(Settings));
			sr.Serialize (tr, Default);
			tr.Close ();
		}
		#endregion 


	}
}

