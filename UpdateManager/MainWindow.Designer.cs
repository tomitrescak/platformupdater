using System;
using Gtk;


namespace UpdateManager
{
	public partial class MainWindow
	{
		CellRendererToggle fileIncludeToggle = new CellRendererToggle() { Activatable = true };
		CellRendererText nameCell = new CellRendererText() { Editable = false };
		CellRendererText colorCell = new CellRendererText() { Editable = false };

		public void InitUi()
		{
			newAction.Activated += CreatePlatform;
			openAction.Activated += OpenPlatform;
			saveAction.Activated += SavePlatform;
			quitAction.Activated += ExitApplication;

			platformPropertiesAction.Activated += ShowPlatformSettings;
			projectPropertiesAction.Activated += ShowProjectSettings;

			addLocalProject.Activated += AddLocalProject;

			platformView.AppendColumn(Catalog.Name, new Gtk.CellRendererText(), "text", 0);

			fileIncludeToggle.Toggled += FileIncludeToggled;
			sourcesView.AppendColumn(Catalog.Include, fileIncludeToggle, "active", 0);
			var nameColumn = sourcesView.AppendColumn(Catalog.Name, colorCell, "text", 1);
			sourcesView.AppendColumn(Catalog.Date, nameCell, "text", 2);
			nameColumn.SetCellDataFunc(colorCell, new Gtk.TreeCellDataFunc(RenderFileName));

			packagesView.AppendColumn(Catalog.Name, nameCell, "text", 1);
			packagesView.AppendColumn(Catalog.Date, nameCell, "text", 2);
			packagesView.AppendColumn(Catalog.Version, nameCell, "text", 3);
			packagesView.AppendColumn(Catalog.VersionInfo, nameCell, "text", 4);

			updatesView.AppendColumn(Catalog.Name, nameCell, "text", 1);
			updatesView.AppendColumn(Catalog.Date, nameCell, "text", 2);
			updatesView.AppendColumn(Catalog.Version, nameCell, "text", 3);
			updatesView.AppendColumn(Catalog.VersionInfo, nameCell, "text", 4);

			createButton.Clicked += CreatePackageUpdate;
			refresh.Clicked += RefreshProject;
			toggleAll.Toggled += ToggleProjectFiles;

			versionInfo.Buffer.Text = Catalog.VersionInfoText;

			platformView.NodeSelection.Changed += LoadProject;

			Gdk.Color col = new Gdk.Color(229,236,238);
			//Gdk.Color.Parse("red", ref col);
			platformView.ModifyBase(StateType.Normal, col);
		
			Log.Info("Initialized!");
		}
		
	}
}

