using System;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Gtk;
using UpdateManager.Model;
using System.IO;
using System.Collections.Generic;
using AutoUpdater;
using EnterpriseDT.Net.Ftp;
using System.Xml.Serialization;
using System.Diagnostics;
using Ionic.Zip;
using System.Threading;

namespace UpdateManager
{
	public partial class MainWindow : Gtk.Window
	{
        static readonly string[] SystemFiles = { "AutoUpdater.dll", "AutoUpdater.pdb", "AutoUpdater.Gui.exe", "AutoUpdater.Gui.pdb", "AutoUpdater.NoGui.exe", "AutoUpdater.NoGui.pdb" };

		const string UpdateFile = "update.xml";

		public static Platform CurrentPlatform { get; private set; }
		public static Project CurrentProject { get; private set; }
        public static MainWindow Instance { get; private set; }

		static readonly SLog.Logger Log = SLog.LogManager.GetCurrentClassLogger();

		FTPConnection _ftp; 

		NodeStore _projectsStore;
		NodeStore _binariesStore;
		NodeStore _packagesStore;
		NodeStore _updatesStore;

		List<VersionInfo> _updatesInfo;
		List<VersionInfo> _packagesInfo;

		AutoResetEvent _waiter = new AutoResetEvent(false);
		bool _waiterResult;

		public MainWindow () : 
				base(Gtk.WindowType.Toplevel)
		{
			this.Build ();
			this.InitUi ();

			// init stores

			_projectsStore = new NodeStore(typeof(GenericNode));
			_binariesStore = new NodeStore(typeof(FileDetailNode));
			_packagesStore = new NodeStore(typeof(SimpleFileInfoNode));
			_updatesStore = new NodeStore(typeof(SimpleFileInfoNode));

			updatesView.NodeStore = _updatesStore;
			packagesView.NodeStore = _packagesStore;
			platformView.NodeStore = _projectsStore;
			sourcesView.NodeStore = _binariesStore;

			// load default project
			try {
				if (!string.IsNullOrEmpty(AppSettings.Default.LastPlatform)) {
					LoadPlatform(AppSettings.Default.LastPlatform);
				}
			} catch (Exception ex) {
				Log.ErrorException (ex, "Error loading platform '{0}': '{1}'", AppSettings.Default.LastPlatform, ex.Message);
				AppSettings.Default.LastPlatform = string.Empty;
				AppSettings.Save ();
			}

		    Instance = this;
		}

		#region Properties
		string FtpProjectDirectory { get { return CurrentPlatform.FtpDir + "/" + CurrentProject.Name; }}
		string FtpPackagesDirectory { get { return FtpProjectDirectory + "/Packages"; }}
		string FtpUpdatesDirectory { get { return FtpProjectDirectory + "/Updates"; }}

        internal string VersionsFile { get { return CurrentProject.Name + ".xml"; } }
		internal string PlatformDirectory { get { return System.IO.Path.GetDirectoryName (CurrentPlatform.Path); }}
        internal string LocalProjectDirectory { get { return System.IO.Path.Combine(PlatformDirectory, CurrentProject.Path); } }
        internal string LocalPackagesDirectory { get { return System.IO.Path.Combine(LocalProjectDirectory, "Packages"); } }
        internal string LocalUpdatesDirectory { get { return System.IO.Path.Combine(LocalProjectDirectory, "Updates"); } }
        internal string ProjectUpdatesVersionInfoFile { get { return System.IO.Path.Combine(LocalUpdatesDirectory, VersionsFile); } }
        internal string ProjectPackagesVersionInfoFile { get { return System.IO.Path.Combine(LocalPackagesDirectory, VersionsFile); } }
        internal string ProjectUpdateInfoFile { get { return System.IO.Path.Combine(LocalUpdatesDirectory, UpdateFile); } }
		#endregion

		#region Platform Methods
		void OpenPlatform(object sender, EventArgs e)
		{
			Gtk.FileChooserDialog fc = new Gtk.FileChooserDialog ("Choose the platform to open",
			                                                      this,
			                                                      FileChooserAction.Open,
			                                                      "Cancel", ResponseType.Cancel,
			                                                      "Open", ResponseType.Accept);
			var filter = new FileFilter ();
			filter.Name = "Platform file";
			//filter.AddMimeType("image/png");
			filter.AddPattern ("*.platform");
			fc.AddFilter (filter);

			if (fc.Run () == (int)ResponseType.Accept) {
				LoadPlatform (fc.Filename);
			}

			//Don't forget to call Destroy() or the FileChooserDialog window won't get closed.
			fc.Destroy ();


		}

		void CreatePlatform(object sender, EventArgs e)
		{
			Gtk.FileChooserDialog fc = new Gtk.FileChooserDialog ("Choose the platform directory",
			                                                      this,
			                                                      FileChooserAction.SelectFolder,
			                                                      "Cancel", ResponseType.Cancel,
			 
			                                                      "Open", ResponseType.Accept);
			if (fc.Run () == (int)ResponseType.Accept) {
				CreatePlatform (System.IO.Path.Combine(fc.Filename, "main.platform"));
			}

			//Don't forget to call Destroy() or the FileChooserDialog window won't get closed.
			fc.Destroy ();
		}

		void SavePlatform(object sender, EventArgs e)
		{
			if (CurrentPlatform != null) {
				CurrentPlatform.Save ();
			}
		}

		void ShowPlatformSettings(object sender, EventArgs e)
		{
			if (CurrentPlatform == null) {
				Log.Info ("Please load the platform first!");
				return;
			}
			// TODO: Make available for applications or sources only
			var settings = new PlatformProperties ("Platform Settings", CurrentPlatform); //, this, DialogFlags.Modal);
			settings.Modal = true;
			settings.Resize (600, 200);
			settings.Show ();
			settings.Saved += () => {
				LoadPlatform (CurrentPlatform.Path);
			};
		}

		// private methods

		private void CreatePlatform(string path) {
			var platform = new Platform ();
			platform.Path = path;
			platform.Save ();

			LoadPlatform (path);
		}

		private void LoadPlatform(string path) {
			CurrentPlatform = Platform.Load (path);

			// init ftp
			try {
				//_ftp = new FTPConnection(CurrentPlatform.FtpUrl, CurrentPlatform.FtpUserName, CurrentPlatform.FtpPassword);
				var server = CurrentPlatform.FtpUrl.StartsWith ("ftp://") ? CurrentPlatform.FtpUrl.Substring (6) : CurrentPlatform.FtpUrl;
				var port = server.IndexOf (':') > 0 ? int.Parse (server.Split (':') [1]) : 21;
				server = server.IndexOf (':') > 0 ? server.Split (':') [0] : server;

				_ftp = new FTPConnection () { ServerAddress = server, UserName = CurrentPlatform.FtpUserName, Password = CurrentPlatform.FtpPassword, AutoLogin = true, ServerPort = port};
				_ftp.Connect ();
			} catch (Exception ex) {
				Log.ErrorException (ex, "Problem connecting to the FTP server!");
			}

			// load projects
			LoadProjects ();

			AppSettings.Default.LastPlatform = path;
			AppSettings.Save ();

            Log.Info("Platform '{0}' loaded", path);
		}

		#endregion

		#region Project Methods

		void AddLocalProject(object sender, EventArgs e) 
		{
			Gtk.FileChooserDialog fc = new Gtk.FileChooserDialog ("Choose the project directory",
			                                                      this,
			                                                      FileChooserAction.SelectFolder,
			                                                      "Cancel", ResponseType.Cancel,
			                                                      "Open", ResponseType.Accept);
			if (fc.Run () == (int)ResponseType.Accept) {
				CreateProject (fc.Filename);
			}

			//Don't forget to call Destroy() or the FileChooserDialog window won't get closed.
			fc.Destroy ();
		}

		void ShowProjectSettings(object sender, EventArgs e)
		{
			if (CurrentProject == null) {
				Log.Info ("Please select the project first!");
				return;
			}
			// TODO: Make available for applications or sources only
			var settings = new ProjectProperties (string.Format("Project '{0}' Settings", CurrentProject.Name), CurrentProject); //, this, DialogFlags.Modal);
			settings.Modal = true;
			settings.Show ();
            settings.Saved += () => {
                // refresh projects
                                        LoadProjects();
				LoadProject ();
			};
		}

		// private methods

		private void CreateProject(string path) {
			if (CurrentPlatform == null)
				return;

			// first create relative path
			if (!path.Contains (PlatformDirectory)) {
				Log.Error ("Cannot add this project as it is not in the platform directory!");
				return;
			}

			var name = System.IO.Path.GetFileNameWithoutExtension(path);
            
            if (CurrentPlatform.Projects.Any(w => w.Name == name))
            {
                Log.Warn("Project with name '{0}' already exists!", name);
                return;
            }
            
            path = path.Replace(PlatformDirectory, "");


			var project = new Project ();
			project.Name = name;
			project.Path = path.TrimStart('\\');

			CurrentPlatform.Projects.Add (project);
            _projectsStore.AddNode(new GenericNode(project.Name));

			platformView.ShowAll ();
			platformView.QueueDraw ();

            Log.Info("Project '{0}' created", name);
		}

		void LoadProjects() 
		{
			try {
				_projectsStore.Clear();

				Gtk.Application.Invoke(delegate {
					foreach (var lib in CurrentPlatform.Projects) {
						_projectsStore.AddNode(new GenericNode(lib.Name));
					}
				});
				Log.Info("Projects loaded.");
			} catch (Exception ex) {
				Log.Warn("Error loading projects: " + ex.Message);
			}
		}

		void LoadProject(object sender, EventArgs e)
		{
			LoadProject ();
		}

		void LoadProject() {
			_binariesStore.Clear ();
			_updatesStore.Clear ();
			_packagesStore.Clear ();
			_packagesInfo = null;
			_updatesInfo = null;

			if (platformView.NodeSelection.SelectedNode == null) {
				return;
			}

			// find local version of this project
			CurrentProject = CurrentPlatform.Projects.Find(w => w.Name == ((GenericNode) platformView.NodeSelection.SelectedNode).Name);			
			var di = new DirectoryInfo (LocalProjectDirectory);

			// load project depending on data from ftp
			try {
				Log.Info("Loading project '{0}'", CurrentProject.Name);

				// load packages
				LoadPackages();

				// load updates
				LoadUpdates();

				// show source files
				RefreshSourceDirectory();

				// fill in the pre and post commands
				if (_updatesInfo != null && _updatesInfo.Count > 0) {
					if (_updatesInfo[0].PreInstallActions != null)
						preParameters.Buffer.Text = string.Join("\n", _updatesInfo[0].PreInstallActions);
					if (_updatesInfo[0].PostInstallActions != null)
						postParameters.Buffer.Text = string.Join("\n", _updatesInfo[0].PostInstallActions);
				}

				Log.Info("Platform loaded.");
			} catch (Exception ex) {
				Log.Warn("Error loading project: " + ex.Message);
			}
		}

		private void LoadPackages()
		{
			try {
				_packagesStore.Clear ();

				var path = System.IO.Path.Combine (LocalPackagesDirectory, VersionsFile);

				Log.Info ("Loading packages information ...");

				// download information about versions
				if (!File.Exists(ProjectPackagesVersionInfoFile)) {
					Log.Warn ("No packages available!");
					return;
				}

				_packagesInfo = LoadVersions (path);
				if (_packagesInfo != null) {
					Gtk.Application.Invoke(delegate {
						foreach (var version in _packagesInfo) {
							_packagesStore.AddNode (new SimpleFileInfoNode (version, LocalPackagesDirectory, _ftp));
						}
					});
				}
				Log.Info ("Package info loaded.");
			} catch (Exception ex) {
				Log.Warn("Error loading packages: " + ex.Message);
			}
		}

		private void LoadUpdates()
		{
			try {

				_updatesStore.Clear ();

				var path = System.IO.Path.Combine (LocalUpdatesDirectory, VersionsFile);

				Log.Info ("Loading updates information ...");
				// download information about versions
				if (!File.Exists(ProjectUpdatesVersionInfoFile)) {
					Log.Warn ("No updates available!");
					return;
				}
				_updatesInfo = LoadVersions (path);
				if (_updatesInfo != null) {
					Gtk.Application.Invoke(delegate {
						foreach (var version in _updatesInfo) {
							_updatesStore.AddNode (new SimpleFileInfoNode (version, LocalUpdatesDirectory, _ftp));
						}
					});
				}
				Log.Info ("Update info loaded.");

			} catch (Exception ex) {
				Log.Warn("Error loading updates: " + ex.Message);
			}
		}

		private static List<VersionInfo> LoadVersions(string path)
		{
			try
			{
				if (!File.Exists(path))
				{
					return null;
				}

				List<VersionInfo> versions;
				using (var fs = new FileStream(path, FileMode.Open))
				{
					var sr = new XmlSerializer(typeof (List<VersionInfo>));
					versions = (List<VersionInfo>) sr.Deserialize(fs);
					fs.Close();
				}
				return versions;
			} catch (Exception ex)
			{
				Log.ErrorException(ex, "Error loading version file: " + ex.Message);
				return null;
			}
		}

		private void RefreshSourceDirectory()
		{
			if (CurrentProject == null)
				return;

            if (string.IsNullOrEmpty(CurrentProject.BinariesRoot))
            {
                Log.Warn("Binaries directory not set!");
                return;
            }
            var binariesDir = System.IO.Path.Combine (LocalProjectDirectory, CurrentProject.BinariesRoot.Trim('\\'));

			if (!Directory.Exists (binariesDir)) {
				Log.Warn ("No access to folder '{0}', please get sources from '{1}' and compile!", CurrentProject.BinariesRoot, CurrentProject.Repository);
			}

			// get all directories in the directory
			var lastUpdate = _updatesInfo != null && _updatesInfo.Count > 0 ? DateTime.ParseExact (_updatesInfo [0].Date, "dd/MM/yyyy HH:mm:ss z", CultureInfo.InvariantCulture) : new DateTime ();
			var directories = new DirectoryInfo (binariesDir).GetDirectories ();

			Gtk.Application.Invoke (delegate {
                _binariesStore.Clear();

				foreach (var lib in directories.OrderByDescending(w => w.LastWriteTime)) {
					if (CurrentProject.Ignores.Any (w => w == lib.Name))
						continue;
					_binariesStore.AddNode (new FileDetailNode (lib, lastUpdate));
				}

				// get all files in the directory
				var files = new DirectoryInfo (binariesDir).GetFiles ();

				foreach (var lib in files.OrderByDescending(w => w.LastWriteTime)) {
					if (CurrentProject.Ignores.Any (w => w == lib.Name))
						continue;
					_binariesStore.AddNode (new FileDetailNode (lib, lastUpdate));
				}
			});
		}		
		#endregion

		#region Package Creation

		void ToggleProjectFiles(object sender, EventArgs e)
		{
		    Gtk.Application.Invoke(delegate
		        {
		            var numer = _binariesStore.GetEnumerator();

		            while (numer.MoveNext())
		            {
		                var node = numer.Current as FileDetailNode;
		                node.Selected = toggleAll.Active;
		            }

		            sourcesView.ShowAll();
		            sourcesView.QueueDraw();
		        });
		}

		void RefreshProject(object sender, EventArgs e)
		{
			RefreshSourceDirectory ();
		}

		void CreateUpdate()
		{
			if (_updatesInfo == null) {
				_updatesInfo = new List<VersionInfo> ();
			}

		    if (CreateZip(_updatesInfo, LocalUpdatesDirectory, FtpUpdatesDirectory, LocalUpdatesDirectory))
		    {
		        LoadUpdates();
		        RefreshSourceDirectory();
		    }
		}

		void CreatePackage()
		{
			if (_packagesInfo == null) {
				_packagesInfo = new List<VersionInfo> ();
			}

		    if (CreateZip(_packagesInfo, LocalPackagesDirectory, FtpPackagesDirectory, LocalPackagesDirectory))
		    {
		        LoadPackages();
		        RefreshSourceDirectory();
		    }
		}

		void CreatePackageUpdate(object sender, EventArgs e)
		{
			if (CurrentPlatform == null || CurrentProject == null) {
				Log.Warn("Please load platform and a project!");
				return;
			}

			if (update.Active) {
				new System.Threading.Thread (CreateUpdate).Start ();
			} else {
				new System.Threading.Thread (CreatePackage).Start ();
			}

		}

		bool CreateZip(List<VersionInfo> versions, string localPath, string serverPath, string httpPath)
		{
            // validate
            if (string.IsNullOrEmpty(CurrentPlatform.HttpRoot))
            {
                Log.Warn("Please assign first the HTTP root of the project");
                return false;
            }

            if (_ftp == null)
            {
                Log.Warn("Please define FTP connection");
                return false;
            }

			try {
				var binariesDir = System.IO.Path.Combine (LocalProjectDirectory, CurrentProject.BinariesRoot.Trim('\\'));
			    var assPath = System.IO.Path.Combine(binariesDir, CurrentProject.VersionFile);
			    var fvi = FileVersionInfo.GetVersionInfo (assPath);
                var fdate = File.GetLastWriteTime(assPath);
			    
                // create version in shape 1.0.0.0
                var tempVer = fvi.FileVersion;
			    var splt = tempVer.Split('.');
                for (var i = 0; i < 4 - splt.Length; i++)
                {
                    tempVer += ".0";
                }

                var localVersion = new Version(tempVer);

				var serverVersion = new Version ("0.0.0.0");
				if (versions.Count > 0) {
					serverVersion = new Version (versions [0].Version);
				}

				var zipName = CurrentProject.Name + "." + localVersion + ".zip";
				var zipPath = System.IO.Path.Combine (localPath, zipName);
                 

				// the update version has to be higher than the previous one!
				if (localVersion < serverVersion) {
					Gtk.Application.Invoke (delegate {
						MessageDialog md = new MessageDialog (this, 
						                                      DialogFlags.DestroyWithParent, MessageType.Warning, 
						                                      ButtonsType.Ok, "Local version is older than the one of server. Please update your project!", localVersion);
						md.Run ();
						md.Destroy ();
					});
					return false;
				}

				// the update version has to be higher than the previous one!
				if (localVersion.Equals (serverVersion)) {
					Gtk.Application.Invoke (delegate
					    {  
						    MessageDialog md = new MessageDialog (this, 
						                                      DialogFlags.DestroyWithParent, MessageType.Warning, 
						                                      ButtonsType.YesNo, "Versions are equal, do you wish to create a service version? Version '{0}'", localVersion);
                        if (md.Run() == (int)ResponseType.Yes)
                        {
                            if (versions.Count > 0)
                            {
                                versions.RemoveAt(0);
                                _waiterResult = true;
                            }
                            else
                            {
                                _waiterResult = false;
                            }
                        }
                        else
                        {
                            return;
                        }
						md.Destroy ();
						_waiter.Set ();
					});
					_waiter.WaitOne ();
					if (!_waiterResult)
						return false;
				}

				// create zip


				using (ZipFile zip = new ZipFile()) {
					var numer = _binariesStore.GetEnumerator ();

					var zippedFiles = 0;
					while (numer.MoveNext()) {
						var node = numer.Current as FileDetailNode;
						if (!node.Selected)
							continue;

						zippedFiles ++;

						if (node.IsDirectory) {
							zip.AddDirectory (System.IO.Path.Combine (binariesDir, node.Name), "/" + node.Name);
						} else {
						    if (SystemFiles.Contains(node.Name))
						    {
						        var path = System.IO.Path.Combine(binariesDir, node.Name);

                                if (!Directory.Exists("ZipTmp"))
                                {
                                    Directory.CreateDirectory("ZipTmp");
                                }
                                File.Copy(path, "ZipTmp/M1234_" + node.Name, true);
                                zip.AddFile("ZipTmp/M1234_" + node.Name, "/");
						    }
						    else
						    {
                                zip.AddFile(System.IO.Path.Combine(binariesDir, node.Name), "/");
						    }
						}
					}

                   

					// check if we are creating at least one file

					if (zippedFiles == 0) {
						Log.Warn ("Please select at least one file!");
						return false;
					}

					Log.Info ("Adding {0} file(s) to the zip file '{1}'", zippedFiles, zipName);
					// make sure the directory exists
					MakeDirectories (zipPath);
					// delete potential exisitn zip
					if (File.Exists (zipPath)) {
						File.Delete (zipPath);
					}
					// create zip
					zip.Save (zipPath);

                    // remove temp dir
                    if (Directory.Exists("ZipTmp"))
                    {
                        Directory.Delete("ZipTmp", true);
                    }
				}

				// update info on current package / update
				var vinfo = new VersionInfo {
                    Date = fdate.ToString("dd/MM/yyyy HH:mm:ss z"),
					Description = versionInfo.Buffer.Text,
					PreInstallActions = preParameters.Buffer.Text.Split('\n').ToList(),
					PostInstallActions = postParameters.Buffer.Text.Split('\n').ToList(),
					Version = localVersion.ToString(4),
					Url = 
					    CurrentPlatform.HttpRoot + 
					    (CurrentPlatform.HttpRoot.EndsWith("/") ? string.Empty : "/") +
					    serverPath.TrimStart('/') + 
					    (serverPath.EndsWith("/") ? string.Empty : "/") + 
					    zipName.TrimStart('/')
				};
				versions.Insert (0, vinfo);

				// save versions file
				var versionsFile = System.IO.Path.Combine (localPath, VersionsFile);
				SaveVersions (versions, versionsFile);
       

                

			    for (var i = 0; i < 3; i++)
			    {
			        try
			        {
                        // connect to ftp
                        if (!_ftp.IsConnected)
                        {
                            _ftp.Connect();
                        }
			            // upload files to server
			            if (!_ftp.WorkingDirectory.EndsWith(serverPath))
			            {
			                var dir = serverPath.Substring(0, serverPath.IndexOf('/'));

                            // if we are not in the good directory, try to find it
			                if (!_ftp.DirectoryExists(dir))
			                {
			                    while (System.IO.Path.GetFileName(_ftp.WorkingDirectory) != dir)
			                    {
			                        _ftp.ChangeWorkingDirectoryUp();
			                    }
			                    _ftp.ChangeWorkingDirectoryUp();
			                }
			                _ftp.ChangeWorkingDirectory(serverPath);
			            }
			            Log.Info("Uploading '{0}'", zipName);
			            _ftp.UploadFile(zipPath, zipName);
			            Log.Info("Uploading '{0}'", VersionsFile);
			            _ftp.UploadFile(versionsFile, VersionsFile);
			            break;
			        }
			        catch (Exception ex)
			        {
			            Log.ErrorException(ex, ex.Message);
			        }
			    }
			    Log.Info ("Successfully created '{0}'", zipName);

			} catch (Exception ex) {
				Log.ErrorException (ex, "Error creating package/update: " + ex.Message);
			    return false;
			}
		    return true;
		}
		#endregion

		#region Application Methods

		void ExitApplication(object sender, EventArgs e)
		{
			Application.Quit ();
		}



		#endregion

		#region UI Methods
		private void RenderFileName(Gtk.TreeViewColumn column, Gtk.CellRenderer cell, Gtk.TreeModel model, Gtk.TreeIter iter)
		{
			var log = model.GetValue(iter, 4);

			if (log.Equals(true))
			{
				(cell as Gtk.CellRendererText).Foreground = "red";
			}
			else 
			{
				(cell as Gtk.CellRendererText).Foreground = "black";
			}
		}

		private void FileIncludeToggled(object o, ToggledArgs args)
		{
			var ctrl = o as Gtk.CellRendererToggle;

			Gtk.TreeIter iter;
			var filterView = _binariesStore.GetNode (new Gtk.TreePath (args.Path)) as FileDetailNode;

			filterView.Selected = !ctrl.Active;
		}

		private void SaveVersions(List<VersionInfo> versions, string path)
		{

			TextWriter tr = new StreamWriter (path);
			var sr = new XmlSerializer (typeof(List<VersionInfo>));
			sr.Serialize (tr, versions);
			tr.Close ();
		}

		private void MakeDirectories(string filePath) {
			var dinfo = new DirectoryInfo(System.IO.Path.GetDirectoryName(filePath));
			dinfo.Create();
		}

		protected void AddToIgnore (object sender, EventArgs e)
		{
			var numer = _binariesStore.GetEnumerator ();

		    while (numer.MoveNext())
		    {
		        var node = numer.Current as FileDetailNode;
		        if (!node.Selected)
		            continue;
		        CurrentProject.Ignores.Add(node.Name);

		    }

            RefreshSourceDirectory();
		}
		#endregion


	}
}

