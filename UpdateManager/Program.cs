﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;
using Gtk;
using System.Reflection;

namespace UpdateManager
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Init();
            var myWin = new MainWindow();
            myWin.Resize(980, 640);
			myWin.Title = "Platform manager " + Assembly.GetExecutingAssembly ().GetName ().Version;
            myWin.Destroyed += new EventHandler(myWin_Destroyed);            
            myWin.ShowAll();
            Application.Run();
        }

        static void myWin_Destroyed(object sender, EventArgs e)
        {
            Application.Quit();
        }
    }
}
