﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Text;
//using GLib;
//using Gtk;
//using UpdateManager.Model;
//using EnterpriseDT.Net.Ftp;
//using AutoUpdater;
//using System.Xml.Serialization;
//using Ionic.Zip;
//using System.Diagnostics;
//using System.Threading;
//
//namespace UpdateManager
//{
//    public partial class MainWindowOld : Window
//    {
//        // fields
//		const string VersionsFile = "versions.xml";
//		const string ProjectFile = "project.info";
//
//		public static Platform CurrentPlatform { get; private set; }
//
//        private static readonly SLog.Logger Log = SLog.LogManager.GetCurrentClassLogger();
//
//        private FTPConnection _ftp;
//        
//        private NodeStore _platformStore;
//        private NodeStore _projectStore;
//		private NodeStore _libraryStore;
//		private NodeStore _packagesStore;
//		private NodeStore _updatesStore;
//
//        private string _currentProject;
//
//		private List<VersionInfo> _updatesInfo;
//		private List<VersionInfo> _packagesInfo;
//		private Project _currentProjectInfo;
//
//		private AutoResetEvent _waiter = new AutoResetEvent(false);
//		private bool _waiterResult;
//
//        // ctors
//
//        #region Constructors
//        public MainWindowOld(IntPtr raw)
//            : base(raw)
//        {
//        }
//
//        public MainWindowOld(WindowType type)
//            : base(type)
//        {
//        }
//
//        public MainWindowOld(string title)
//            : base(title)
//        {
//            // initialize interface
//            Initialize();
//
//			_platformStore = new NodeStore(typeof(GenericNode));
//			_projectStore = new NodeStore(typeof(FileDetailNode));
//			_libraryStore = new NodeStore(typeof(SimpleFileInfoNode));
//			_packagesStore = new NodeStore(typeof(SimpleFileInfoNode));
//			_updatesStore = new NodeStore(typeof(SimpleFileInfoNode));
//
//			updatesView.NodeStore = _updatesStore;
//			packagesView.NodeStore = _packagesStore;
//			libraryView.NodeStore = _libraryStore;
//			platformView.NodeStore = _platformStore;
//			projectView.NodeStore = _projectStore;
//
//
//
//			// load default project
//			try {
//			if (!string.IsNullOrEmpty(AppSettings.Default.LastPlatform)) {
//				LoadPlatform(AppSettings.Default.LastPlatform);
//			}
//			} catch {
//			}
//        }
//        #endregion
//
//		// properties
//
//		private string FtpLibraryDirectory { get { return CurrentPlatform.FtpDir + "/Library/"; }}
//		private string FtpProjectDirectory { get { return CurrentPlatform.FtpDir + "/" + _currentProject; }}
//		private string FtpPackagesDirectory { get { return FtpProjectDirectory + "/Packages"; }}
//		private string FtpUpdatesDirectory { get { return FtpProjectDirectory + "/Updates"; }}
//
//		private string LocalLibraryDirectory { get { return System.IO.Path.Combine(CurrentPlatform.Path, "Library"); }}
//		private string LocalProjectDirectory { get { return System.IO.Path.Combine(CurrentPlatform.SourcesDirectory, _currentProject); }}
//		private string LocalPackagesDirectory { get { return System.IO.Path.Combine(LocalProjectDirectory, "Packages"); }}
//		private string LocalUpdatesDirectory { get { return System.IO.Path.Combine(LocalProjectDirectory, "Updates"); }}
//
//
//
//		// private methods
//
//		#region LoadPlatform(string path)
//		private void LoadPlatform(string path)
//		{
//
//			CurrentPlatform = Platform.Load (path);
//
//			try {
//				//_ftp = new FTPConnection(CurrentPlatform.FtpUrl, CurrentPlatform.FtpUserName, CurrentPlatform.FtpPassword);
//				var server = CurrentPlatform.FtpUrl.StartsWith ("ftp://") ? CurrentPlatform.FtpUrl.Substring (6) : CurrentPlatform.FtpUrl;
//				var port = server.IndexOf (':') > 0 ? int.Parse (server.Split (':') [1]) : 21;
//				server = server.IndexOf (':') > 0 ? server.Split (':') [0] : server;
//				
//				_ftp = new FTPConnection () { ServerAddress = server, UserName = CurrentPlatform.FtpUserName, Password = CurrentPlatform.FtpPassword, AutoLogin = true, ServerPort = port};
//				_ftp.Connect ();
//			} catch (Exception ex) {
//				Log.ErrorException (ex, "Problem connecting to the FTP server!");
//				return;
//			}
//
//			// TODO: Send NOOP command to keep connection alive
//			// fill all the projects from ftp
//
//			// load ftp data
//
//			LoadPlatform ();
//
//			AppSettings.Default.LastPlatform = path;
//			AppSettings.Save ();
//		}
//		#endregion
//
//		#region LoadProject(object sender, EventArgs e)
//		private void LoadProject(object sender, EventArgs e)
//		{
//			_projectStore.Clear ();
//			_updatesStore.Clear ();
//			_packagesStore.Clear ();
//			_packagesInfo = null;
//			_updatesInfo = null;
//
//			if (platformView.NodeSelection.SelectedNode == null) {
//				return;
//			}
//
//			// find local version of this project
//			_currentProject = ((GenericNode)platformView.NodeSelection.SelectedNode).Name;			
//			var di = new DirectoryInfo (System.IO.Path.Combine (CurrentPlatform.SourcesDirectory, _currentProject));
//
//			// check if we want to download this project
//			if (!di.Exists) {
//				Log.Info ("Project '{0}' has not been yet downloaded", _currentProject);
//				return;
//			}
//
//			// load project depending on data from ftp
//			LoadProjectAsync ();
//		}
//		#endregion
//
//		// ftp actions
//
//		#region LoadPlatform()
//		private void LoadPlatform()
//		{
//			new System.Threading.Thread (() => {
//				LoadProjects ();
//				LoadLibrary (); }).Start ();
//		}
//		#endregion
//
//		#region LoadProjectsAsync()
//		private void LoadProjectsAsync() {
//			new System.Threading.Thread(LoadProjects).Start();
//		}
//		
//		private void LoadProjects() {
//			try {
//				Log.Info("Scanning platform for projects ...");
//				
//				_platformStore.Clear();
//				
//				_ftp.ChangeWorkingDirectory(CurrentPlatform.FtpDir);
//				var libList = _ftp.GetFileInfos();// (FtpLibraryDirectory);
//
//				Gtk.Application.Invoke(delegate {
//					foreach (var lib in libList) {
//						if (string.IsNullOrEmpty(lib.Name) || !lib.Dir || lib.Name == "Library") continue;
//						
//						_platformStore.AddNode(new GenericNode(lib.Name));
//					}
//				});
//
//
//				Log.Info("Platform loaded.");
//			} catch (Exception ex) {
//				Log.Warn("Error loading platform: " + ex.Message);
//			}
//		}
//		#endregion
//
//		#region LoadProjectAsync()
//		private void LoadProjectAsync() {
//			new System.Threading.Thread(LoadProject).Start();
//		}
//		
//		private void LoadProject() {
//			try {
//				Log.Info("Loading project '{0}'", _currentProject);
//				
//				// get info about the project
//				_ftp.ChangeWorkingDirectory(FtpProjectDirectory);
//				if (!_ftp.Exists(ProjectFile)) {
//					Log.Error("Project file is missing for project '{0}'", _currentProject);
//					return;
//				}
//
//				var lpath = System.IO.Path.Combine(LocalProjectDirectory, ProjectFile);
//
//				_ftp.DownloadFile(lpath, ProjectFile);
//				_currentProjectInfo = LoadProject(lpath);
//
//
//				// load packages
//				LoadPackages();
//
//				// load updates
//				LoadUpdates();
//
//				// show source files
//				RefreshSourceDirectory();
//
//				// fill in the pre and post commands
//				if (_updatesInfo != null && _updatesInfo.Count > 0) {
//					if (_updatesInfo[0].PreInstallActions != null)
//						preParameters.Buffer.Text = string.Join("\n", _updatesInfo[0].PreInstallActions);
//					if (_updatesInfo[0].PostInstallActions != null)
//						postParameters.Buffer.Text = string.Join("\n", _updatesInfo[0].PostInstallActions);
//				}
//				
//				Log.Info("Platform loaded.");
//			} catch (Exception ex) {
//				Log.Warn("Error loading platform: " + ex.Message);
//			}
//		}
//		#endregion
//
//		#region LoadLibraryAsync()
//		private void LoadLibraryAsync() {
//			new System.Threading.Thread(LoadLibrary).Start();
//			//Gtk.Application.Invoke(delegate { LoadLibraryAsync(); });
//		}
//
//		private void LoadLibrary() {
//			try {
//				Log.Info("Reading library ...");
//
//				_libraryStore.Clear();
//
//				_ftp.ChangeWorkingDirectory(FtpLibraryDirectory);
//				var libList = _ftp.GetFileInfos();// (FtpLibraryDirectory);
//			
//				Gtk.Application.Invoke(delegate {
//					foreach (var lib in libList) {
//						if (string.IsNullOrEmpty(lib.Name) && !lib.Dir) continue;
//
//						_libraryStore.AddNode(new SimpleFileInfoNode(true, LocalLibraryDirectory, FtpLibraryDirectory, _ftp, lib.Name));
//					}
//					libraryView.ShowAll();				
//				});
//				Log.Info("Library loaded.");
//			} catch (Exception ex) {
//				Log.Warn("Error loading library: " + ex.Message);
//			}
//		}
//		#endregion
//
//		#region LoadPackagesAsync()
//		private void LoadPackagesAsync()
//		{
//			new System.Threading.Thread (LoadPackages).Start ();
//		}
//
//		private void LoadPackages()
//		{
//			try {
//				_packagesStore.Clear ();
//
//				var path = System.IO.Path.Combine (LocalPackagesDirectory, VersionsFile);
//
//				Log.Info ("Loading packages information ...");
//				// download information about versions
//				_ftp.ChangeWorkingDirectory (FtpPackagesDirectory);
//				if (!_ftp.Exists (VersionsFile)) {
//					Log.Warn ("No package information stored on server!");
//					return;
//				}
//				_ftp.DownloadFile (path, VersionsFile);
//
//				_packagesInfo = LoadVersions (path);
//				if (_packagesInfo != null) {
//					Gtk.Application.Invoke(delegate {
//						foreach (var version in _packagesInfo) {
//							_packagesStore.AddNode (new SimpleFileInfoNode (version, LocalPackagesDirectory, _ftp));
//						}
//					});
//				}
//				Log.Info ("Package info loaded.");
//			} catch (Exception ex) {
//				Log.Warn("Error loading packages: " + ex.Message);
//			}
//		}
//		#endregion
//
//		#region LoadUpdatesAsync()
//		private void LoadUpdatesAsync()
//		{
//			new System.Threading.Thread (LoadUpdates).Start ();
//		}
//
//		private void LoadUpdates()
//		{
//			try {
//
//				_updatesStore.Clear ();
//				
//				var path = System.IO.Path.Combine (LocalUpdatesDirectory, VersionsFile);
//				
//				Log.Info ("Loading updates information ...");
//				// download information about versions
//				_ftp.ChangeWorkingDirectory (FtpUpdatesDirectory);
//				if (!_ftp.Exists (VersionsFile)) {
//					Log.Warn ("No update information stored on server!");
//					return;
//				}
//				_ftp.DownloadFile (path, VersionsFile);
//				
//				_updatesInfo = LoadVersions (path);
//				if (_updatesInfo != null) {
//					Gtk.Application.Invoke(delegate {
//						foreach (var version in _updatesInfo) {
//
//							_updatesStore.AddNode (new SimpleFileInfoNode (version, LocalUpdatesDirectory, _ftp));
//						}
//					});
//				}
//
//				Log.Info ("Update info loaded.");
//
//			} catch (Exception ex) {
//				Log.Warn("Error loading updates: " + ex.Message);
//			}
//		}
//		#endregion
//
//		#region RefreshSourceDirectory()
//		private void RefreshSourceDirectory()
//		{
//			_projectStore.Clear ();
//
//			if (_currentProjectInfo == null)
//				return;
//
//			var binariesDir = System.IO.Path.Combine (LocalProjectDirectory, _currentProjectInfo.BinariesRoot);
//			
//			if (!Directory.Exists (binariesDir)) {
//				Log.Warn ("No access to folder '{0}', please get sources from '{1}' and compile!", _currentProjectInfo.BinariesRoot, _currentProjectInfo.Repository);
//			}
//
//			// get all directories in the directory
//			var lastUpdate = _updatesInfo != null && _updatesInfo.Count > 0 ? DateTime.Parse (_updatesInfo [0].Date) : new DateTime ();
//			var directories = new DirectoryInfo (binariesDir).GetDirectories ();
//
//			Gtk.Application.Invoke (delegate {
//				foreach (var lib in directories.OrderByDescending(w => w.LastWriteTime)) {
//					if (_currentProjectInfo.Ignores.Any (w => w == lib.Name))
//						continue;
//					_projectStore.AddNode (new FileDetailNode (lib, lastUpdate));
//				}
//				
//				// get all files in the directory
//				var files = new DirectoryInfo (binariesDir).GetFiles ();
//				
//				foreach (var lib in files.OrderByDescending(w => w.LastWriteTime)) {
//					if (_currentProjectInfo.Ignores.Any (w => w == lib.Name))
//						continue;
//					_projectStore.AddNode (new FileDetailNode (lib, lastUpdate));
//				}
//			});
//		}
//		#endregion
//
//		// events
//
//		#region DownloadLibraryFile(object o, ToggledArgs args)
//		void DownloadLibraryFile(object o, ToggledArgs args)
//		{
//			var filterView = _libraryStore.GetNode (new Gtk.TreePath (args.Path)) as SimpleFileInfoNode;
//			DownloadFileOrDirectory (filterView);
//		}
//		#endregion
//
//		#region DownloadPackage(object o, ToggledArgs args)
//		void DownloadPackage(object o, ToggledArgs args)
//		{
//			var filterView = _packagesStore.GetNode (new Gtk.TreePath (args.Path)) as SimpleFileInfoNode;
//			DownloadFileOrDirectory (filterView);
//		}
//		#endregion
//
//		#region DownloadUpdate(object o, ToggledArgs args)
//		void DownloadUpdate(object o, ToggledArgs args)
//		{
//			var filterView = _updatesStore.GetNode (new Gtk.TreePath (args.Path)) as SimpleFileInfoNode;
//			DownloadFileOrDirectory (filterView);
//		}
//		#endregion
//
//		#region DownloadFileOrDirectory(SimpleFileInfoNode node)
//		void DownloadFileOrDirectory(SimpleFileInfoNode node)
//		{
//			MessageDialog md = new MessageDialog (this, 
//			                                     DialogFlags.DestroyWithParent, MessageType.Question, 
//			                                     ButtonsType.YesNo, "Are you sure to want to perform this action?");
//			if (md.Run () == (int)ResponseType.Yes) {
//				node.Toggle ();
//			}
//			md.Destroy ();
//		}
//		#endregion
//
//		#region FileIncludeToggled(object o, ToggledArgs args)
//		private void FileIncludeToggled(object o, ToggledArgs args)
//		{
//			var ctrl = o as Gtk.CellRendererToggle;
//			
//			Gtk.TreeIter iter;
//			var filterView = _projectStore.GetNode (new Gtk.TreePath (args.Path)) as FileDetailNode;
//			
//			filterView.Selected = !ctrl.Active;
//		}
//		#endregion
//
//		#region ToggleProjectFiles(object sender, EventArgs e)
//		void ToggleProjectFiles(object sender, EventArgs e)
//		{
//			var numer = _projectStore.GetEnumerator ();
//
//			while (numer.MoveNext()) {
//				var node = numer.Current as FileDetailNode;
//				node.Selected = toggleAll.Active;
//			}
//
//			projectView.ShowAll ();
//			projectView.QueueDraw ();
//		}
//		#endregion
//		
//		#region RefreshProject(object sender, EventArgs e)
//		void RefreshProject(object sender, EventArgs e)
//		{
//			new System.Threading.Thread (RefreshSourceDirectory).Start ();
//		}
//		#endregion
//
//		#region CreateUpdate()
//		void CreateUpdate()
//		{
//			if (_updatesInfo == null) {
//				_updatesInfo = new List<VersionInfo> ();
//			}
//
//			CreateZip (_updatesInfo, LocalUpdatesDirectory, FtpUpdatesDirectory, _currentProject + "/Updates");
//
//			LoadUpdates ();
//			RefreshSourceDirectory ();
//		}
//		#endregion
//
//		#region CreatePackage()
//		void CreatePackage()
//		{
//			if (_packagesInfo == null) {
//				_packagesInfo = new List<VersionInfo> ();
//			}
//			CreateZip (_packagesInfo, LocalPackagesDirectory, FtpPackagesDirectory, _currentProject + "/Packages");
//
//
//			LoadPackages ();
//			RefreshSourceDirectory ();
//		}
//		#endregion
//
//		#region CreateZip(List<VersionInfo> versions, string localPath, string serverPath)
//		void CreateZip(List<VersionInfo> versions, string localPath, string serverPath, string httpPath)
//		{
//			try {
//				var binariesDir = System.IO.Path.Combine (LocalProjectDirectory, _currentProjectInfo.BinariesRoot);
//				var fvi = FileVersionInfo.GetVersionInfo (System.IO.Path.Combine (binariesDir, _currentProjectInfo.VersionFile));
//				var localVersion = new Version (fvi.FileVersion);
//				var serverVersion = new Version ("0.0.0.0");
//				if (versions.Count > 0) {
//					serverVersion = new Version (versions [0].Version);
//				}
//
//				var zipName = _currentProject + "." + localVersion + ".zip";
//				var zipPath = System.IO.Path.Combine (localPath, zipName);
//
//
//				// the update version has to be higher than the previous one!
//				if (localVersion < serverVersion) {
//					Gtk.Application.Invoke (delegate {
//						MessageDialog md = new MessageDialog (this, 
//						                                      DialogFlags.DestroyWithParent, MessageType.Warning, 
//						                                      ButtonsType.Ok, "Local version is older than the one of server. Please update your project!", localVersion);
//						md.Run ();
//						md.Destroy ();
//					});
//					return;
//				}
//
//				// the update version has to be higher than the previous one!
//				if (localVersion.Equals (serverVersion)) {
//					Gtk.Application.Invoke (delegate {
//						MessageDialog md = new MessageDialog (this, 
//						                                      DialogFlags.DestroyWithParent, MessageType.Warning, 
//						                                      ButtonsType.YesNo, "Versions are equal, do you wish to overwrite the actual version? Version '{0}'", localVersion);
//						if (md.Run () == (int)ResponseType.Yes) {
//							if (versions.Count > 0) {
//								versions.RemoveAt (0);
//								_waiterResult = true;
//							} else {
//								_waiterResult = false;
//							}
//						} 
//						md.Destroy ();
//						_waiter.Set ();
//					});
//					_waiter.WaitOne ();
//					if (!_waiterResult)
//						return;
//				}
//				
//				// create zip
//
//
//				using (ZipFile zip = new ZipFile()) {
//					var numer = _projectStore.GetEnumerator ();
//					
//					var zippedFiles = 0;
//					while (numer.MoveNext()) {
//						var node = numer.Current as FileDetailNode;
//						if (!node.Selected)
//							continue;
//						
//						zippedFiles ++;
//						
//						if (node.IsDirectory) {
//							zip.AddDirectory (System.IO.Path.Combine (binariesDir, node.Name), "/");
//						} else {
//							zip.AddFile (System.IO.Path.Combine (binariesDir, node.Name), "/");
//						}
//					}
//					
//					// check if we are creating at least one file
//					
//					if (zippedFiles == 0) {
//						Log.Warn ("Please select at least one file!");
//						return;
//					}
//					
//					Log.Info ("Adding {0} file(s) to the zip file '{1}'", zippedFiles, zipName);
//					// make sure the directory exists
//					MakeDirectories (zipPath);
//					// delete potential exisitn zip
//					if (File.Exists (zipPath)) {
//						File.Delete (zipPath);
//					}
//					// create zip
//					zip.Save (zipPath);
//				}
//				
//				// update info on current package / update
//				var vinfo = new VersionInfo {
//					Date = DateTime.Now.ToString("dd/MM/yyyy HH:mm"),
//					Description = versionInfo.Buffer.Text,
//					PreInstallActions = preParameters.Buffer.Text.Split('\n').ToList(),
//					PostInstallActions = postParameters.Buffer.Text.Split('\n').ToList(),
//					Version = localVersion.ToString(),
//					Url = 
//					CurrentPlatform.HttpRoot + 
//					(CurrentPlatform.HttpRoot.EndsWith("/") ? string.Empty : "/") +
//					httpPath.TrimStart('/') + 
//					(serverPath.EndsWith("/") ? string.Empty : "/") + 
//					zipName.TrimStart('/')
//				};
//				versions.Insert (0, vinfo);
//
//				// save versions file
//				var versionsFile = System.IO.Path.Combine (localPath, VersionsFile);
//				SaveVersions (versions, versionsFile);
//
//
//
//				// upload files to server
//				_ftp.ChangeWorkingDirectory (serverPath);
//				Log.Info ("Uploading '{0}'", zipName);
//				_ftp.UploadFile (zipPath, zipName);
//				Log.Info ("Uploading '{0}'", VersionsFile);
//				_ftp.UploadFile (versionsFile, VersionsFile);
//
//				Log.Info ("Successfully created '{0}'", zipName);
//					
//			} catch (Exception ex) {
//				Log.ErrorException (ex, "Error creating package/update: " + ex.Message);
//			}
//		}
//		#endregion
//
//		#region CreatePackageUpdate(object sender, EventArgs e)
//		void CreatePackageUpdate(object sender, EventArgs e)
//		{
//			if (CurrentPlatform == null || _currentProject == null) {
//				Log.Warn("Please load platform and a project!");
//				return;
//			}
//
//			if (packageUpdateSelector.Active == -1) {
//				Log.Warn ("Please select what do you want to create package/update");
//				return;
//			}
//
//			var creatingUpdate = packageUpdateSelector.Active == 0;
//			if (creatingUpdate) {
//				new System.Threading.Thread (CreateUpdate).Start ();
//			} else {
//				new System.Threading.Thread (CreatePackage).Start ();
//			}
//
//		}
//		#endregion
//
//		#region ShowSettings(object sender, EventArgs e)
//		void ShowSettings(object sender, EventArgs e)
//		{
//			if (CurrentPlatform == null) {
//				Log.Info ("Please load the platform first!");
//				return;
//			}
//			// TODO: Make available for applications or sources only
//			var settings = new SettingsWindow ("Settings", CurrentPlatform); //, this, DialogFlags.Modal);
//			settings.Modal = true;
//			settings.Resize (600, 200);
//			settings.Show ();
//			settings.Saved += () => {
//				LoadPlatform (CurrentPlatform.Path);
//			};
//		}
//		#endregion
//
//		#region ExitApplication(object sender, EventArgs e)
//		void ExitApplication(object sender, EventArgs e)
//		{
//			Application.Quit ();
//		}
//		#endregion
//		
//		#region SavePlatform(object sender, EventArgs e)
//		void SavePlatform(object sender, EventArgs e)
//		{
//			if (CurrentPlatform != null) {
//				CurrentPlatform.Save ();
//			} else {
////				CurrentPlatform = new Platform ();
////				CurrentPlatform.Path = "test.platform";
////				CurrentPlatform.Save ();
//			}
//		}
//		#endregion
//		
//		#region OpenPlatform(object sender, EventArgs e)
//		void OpenPlatform(object sender, EventArgs e)
//		{
//			Gtk.FileChooserDialog fc = new Gtk.FileChooserDialog ("Choose the platform to open",
//			                                                      this,
//			                                                      FileChooserAction.Open,
//			                                                      "Cancel", ResponseType.Cancel,
//			                                                      "Open", ResponseType.Accept);
//			var filter = new FileFilter ();
//			filter.Name = "Platform file";
//			//filter.AddMimeType("image/png");
//			filter.AddPattern ("*.platform");
//			fc.AddFilter (filter);
//			
//			if (fc.Run () == (int)ResponseType.Accept) {
//				LoadPlatform (fc.Filename);
//			}
//			
//			//Don't forget to call Destroy() or the FileChooserDialog window won't get closed.
//			fc.Destroy ();
//
//
//		}
//		#endregion
//
//		#region LoadVersions()
//		private static List<VersionInfo> LoadVersions(string path)
//		{
//			try
//			{
//				if (!File.Exists(path))
//				{
//					return null;
//				}
//				
//				List<VersionInfo> versions;
//				using (var fs = new FileStream(path, FileMode.Open))
//				{
//					var sr = new XmlSerializer(typeof (List<VersionInfo>));
//					versions = (List<VersionInfo>) sr.Deserialize(fs);
//					fs.Close();
//				}
//				return versions;
//			} catch (Exception ex)
//			{
//				Log.ErrorException(ex, "Error loading version file: " + ex.Message);
//				return null;
//			}
//		}
//		#endregion
//
//		#region LoadProject()
//		private static Project LoadProject(string path)
//		{
//			try
//			{
//				if (!File.Exists(path))
//				{
//					return null;
//				}
//				
//				Project project;
//				using (var fs = new FileStream(path, FileMode.Open))
//				{
//					var sr = new XmlSerializer(typeof (Project));
//					project = (Project) sr.Deserialize(fs);
//					fs.Close();
//				}
//				return project;
//			} catch (Exception ex)
//			{
//				Log.ErrorException(ex, "Error loading project file: " + ex.Message);
//				return null;
//			}
//		}
//		#endregion
//    
//		#region SaveProject(Project proj)
//		private void SaveProject(Project proj)
//		{
//			TextWriter tr = new StreamWriter ("project.info");
//			var sr = new XmlSerializer (typeof(Project));
//			sr.Serialize (tr, proj);
//			tr.Close ();
//		}
//		#endregion
//
//		#region SaveVersions(List<VersionInfo> versions)
//		private void SaveVersions(List<VersionInfo> versions, string path)
//		{
//			TextWriter tr = new StreamWriter (path);
//			var sr = new XmlSerializer (typeof(List<VersionInfo>));
//			sr.Serialize (tr, versions);
//			tr.Close ();
//		}
//		#endregion
//	
//		private void MakeDirectories(string filePath) {
//			var dinfo = new DirectoryInfo(System.IO.Path.GetDirectoryName(filePath));
//			dinfo.Create();
//		}
//	}
//}
