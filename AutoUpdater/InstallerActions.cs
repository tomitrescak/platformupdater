﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using AutoUpdater.Logging;

namespace AutoUpdater
{
    public class InstallerActions
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
 
        //[ImportMany(typeof(IInstallerAction))]
        //public IEnumerable<IInstallerAction> Events { get; set; }

        public InstallerActions(string catalogSource)
        {
            //var path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), catalogSource);
            //var catalog = new AggregateCatalog();
            //catalog.Catalogs.Add(new DirectoryCatalog(path));

            
            //var container = new CompositionContainer(catalog);
            //container.ComposeParts(this);

            //Log.Info("Found '{0}' actions.", Events.Count());
        }

        public bool ExectutePreCommands(VersionInfo version)
        {
            foreach (var preInstallAction in version.PreInstallActions)
            {
                if (string.IsNullOrEmpty(preInstallAction)) continue;
                if (!ExecuteCommand(preInstallAction)) return false; 
            }

            //foreach (var installerEvent in Events)
            //{
            //    if (installerEvent.PreInstallCommands != null)
            //    {
            //        foreach (var preCommand in installerEvent.PreInstallCommands)
            //        {
            //            if (!ExecuteCommand(preCommand))
            //            {
            //                return false;
            //            }
            //        }
            //    }
            //}
            return true;
        }

        public bool ExectutePreActions()
        {
            //foreach (var installerEvent in Events)
            //{
            //    if (!installerEvent.PreExecute())
            //    {
            //        return false;
            //    }
            //}
            return true;
        }

        public bool ExectutePostCommands(VersionInfo version)
        {
            foreach (var action in version.PostInstallActions)
            {
                if (string.IsNullOrEmpty(action)) continue;
                if (!ExecuteCommand(action)) return false;
            }

            //foreach (var installerEvent in Events)
            //{
            //    if (installerEvent.PostInstallCommands != null)
            //    {
            //        foreach (var preCommand in installerEvent.PostInstallCommands)
            //        {
            //            if (!ExecuteCommand(preCommand))
            //            {
            //                return false;
            //            }
            //        }
            //    }
            //}
            return true;
        }

        public bool ExectutePostActions()
        {
            //foreach (var installerEvent in Events)
            //{
            //    if (!installerEvent.PostExecute())
            //    {
            //        return false;
            //    }
            //}
            return true;
        }

        public static bool ExecuteCommand(string command)
        {
            try
            {
                // Start the child process.
                var p = new Process();
                // Redirect the output stream of the child process.
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.RedirectStandardError = true;

                p.StartInfo.FileName = command.Split(' ')[0];
                if (command.IndexOf(' ') > 0)
                {
                    p.StartInfo.Arguments = command.Substring(command.IndexOf(' ') + 1);
                }

                p.Start();
                // Do not wait for the child process to exit before
                // reading to the end of its redirected stream.
                // p.WaitForExit();
                // Read the output stream first and then wait.
                var output = p.StandardOutput.ReadToEnd();
                Log.Info(output);
                var error = p.StandardError.ReadToEnd();
                p.WaitForExit();
                if (!string.IsNullOrEmpty(error))
                {
                    Log.Error(error);
                    return false;
                }
                return true;
            } catch (Exception ex)
            {
                Log.ErrorException(ex, ex.Message);
                return false;
            }

        }
    }
}
