using System;

namespace AutoUpdater
{
	public interface IUpdaterGui
	{
		void ShowDialog(string message);
		void ShowMessage(string message);
		void ShowProgress(int progress, int total);
		void Quit();
		bool Ask(string question);
	}
}

