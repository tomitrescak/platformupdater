using System;
using System.Collections.Generic;
using System.Reflection;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;
using AutoUpdater.Logging;
using Ionic.Zip;

namespace AutoUpdater
{
    public class UpdateHandler
    {
        
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public static bool UseGui;
        private static readonly string AppDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        // private static string Updater = UseGui ? "AutoUpdater.Gui.exe" : "AutoUpdater.NoGui.exe";
        private static string UpdateFolder = Path.Combine(AppDir, "Updates");

        private static string _updateUrl;

        #region GetUpdateInfo(string downloadsURL)
        /// <summary>Get update and version information from specified online file - returns a List</summary>
        /// <param name="downloadsURL">URL to download file from</param>
        /// <returns>List containing the information from the pipe delimited version file</returns>
        public static List<VersionInfo> GetUpdateInfo(string downloadsURL)
        {
            _updateUrl = downloadsURL;

            //create download folder if it does not exist
            if (!Directory.Exists(UpdateFolder))
            {
                Directory.CreateDirectory(UpdateFolder);
            }

            //let's try and download update information from the web
            //if the download of the file was successful
            return Webdata.DownloadFromWeb(downloadsURL, UpdateFolder) ?
                LoadVersions() :
                null;
        } 
        #endregion

        #region InstallUpdateNow(string downloadsUrl, string downloadTo, bool unzip)
        /// <summary>Download file from the web immediately</summary>
        /// <param name="downloadsUrl">URL to download file from</param>
        /// <param name="filename">Name of the file to download</param>
        /// <param name="downloadTo">Folder on the local machine to download the file to</param>
        /// <param name="unzip">Unzip the contents of the file</param>
        /// <returns>Void</returns>
        public static void InstallUpdateNow(string downloadsUrl, string downloadTo, bool unzip)
        {

            if (Webdata.DownloadFromWeb(downloadsUrl, downloadTo))
            {
                if (unzip)
                {
                    UnZip(Path.Combine(downloadTo, Path.GetFileName(downloadsUrl)), downloadTo);
                }
            }

        } 
        #endregion

        #region InstallUpdateRestart(string downloadsUrl, string appName, bool delete = true)
        /// <summary>Starts the update application passing across relevant information</summary>
        /// <param name="downloadsUrl">URL to download file from</param>
        /// <returns>Void</returns>
        public static void InstallUpdateRestart(List<VersionInfo> versions, string appLauncher, bool delete = true, bool useGui = true, bool kill = true, bool launch = true)
        {
            var updater = useGui ? "AutoUpdater.Gui.exe" : "AutoUpdater.NoGui.exe"; 

            var mono = Type.GetType("Mono.Runtime") != null;

            // in .net we execute directly exe
            // in mono we call process with startup arguments

            var cmdLn = mono ? (updater + " ") : "";

            // we start from latest to newest update
            versions.Reverse();

            foreach (var version in versions)
            {
                cmdLn += " --version " + version.Version;
            }

			cmdLn += " --process \"" + (mono ? "mono" : Path.GetFileName(appLauncher)) + "\"";
			cmdLn += " --app \"" + appLauncher + "\"";
            cmdLn += " --delete " + delete;
            cmdLn += " --kill " + kill;
            cmdLn += " --launch " + launch;
            cmdLn += " --updaterFile " + Path.GetFileName(_updateUrl);

            var startInfo = new ProcessStartInfo
                                {
                                    FileName = mono ? "mono" : updater,
                                    Arguments = cmdLn
                                };
            startInfo.WorkingDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            
            Log.Debug("Starting '{0} {1}'", startInfo.FileName, startInfo.Arguments);

            Process.Start(startInfo);
        } 
        #endregion

        #region LoadVersions()
        private static List<VersionInfo> LoadVersions()
        {
            var path = Path.Combine(UpdateFolder, Path.GetFileName(_updateUrl));
            if (!File.Exists(path))
            {
                return null;
            }

            List<VersionInfo> versions;
            using (var fs = new FileStream(path, FileMode.Open))
            {
                var sr = new XmlSerializer(typeof(List<VersionInfo>));
                versions = (List<VersionInfo>)sr.Deserialize(fs);
                fs.Close();
            }
            return versions;
        } 
        #endregion

        #region UnZip(string file, string unZipTo)//, bool deleteZipOnCompletion)
        private static bool UnZip(string file, string unZipTo)//, bool deleteZipOnCompletion)
        {
            try
            {

                // Specifying Console.Out here causes diagnostic msgs to be sent to the Console
                // In a WinForms or WPF or Web app, you could specify nothing, or an alternate
                // TextWriter to capture diagnostic messages. 

                using (ZipFile zip = ZipFile.Read(file))
                {
                    // This call to ExtractAll() assumes:
                    //   - none of the entries are password-protected.
                    //   - want to extract all entries to current working directory
                    //   - none of the files in the zip already exist in the directory;
                    //     if they do, the method will throw.
                    zip.ExtractAll(unZipTo);
                }

                //if (deleteZipOnCompletion) File.Delete(unZipTo + file);

            }
            catch (System.Exception)
            {
                return false;
            }

            return true;
        } 
        #endregion

        ///// <summary>Updates the update application by renaming prefixed files</summary>
        ///// <param name="updaterPrefix">Prefix of files to be renamed</param>
        ///// <param name="containingFolder">Folder on the local machine where the prefixed files exist</param>
        ///// <returns>Void</returns>
        //public static void UpdateMe(string updaterPrefix, string containingFolder)
        //{

        //    DirectoryInfo dInfo = new DirectoryInfo(containingFolder);
        //    FileInfo[] updaterFiles = dInfo.GetFiles(updaterPrefix + "*");
        //    int fileCount = updaterFiles.Length;

        //    foreach (FileInfo file in updaterFiles)
        //    {
        //        string newFile = Path.Combine(containingFolder, file.Name);
        //        string origFile = Path.Combine(containingFolder, file.Name.Substring(updaterPrefix.Length, file.Name.Length - updaterPrefix.Length));

        //        if (File.Exists(origFile)) { File.Delete(origFile); }

        //        File.Move(newFile, origFile);

        //    }

        //}



    }
}

