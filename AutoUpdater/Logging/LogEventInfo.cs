﻿using System;
using System.Threading;
using System.Xml.Serialization;

namespace AutoUpdater.Logging
{
    public enum ProgressState
    {
        Running,
        Done, 
        DoneAndCopy,
        Error,
        ErrorAndCopy
    }

    [XmlRoot]
    public class LogEventInfo
    {
        private static object _locker = new object();
        private static long _id = 0;

        
        [XmlAttribute]
        public long Id { get; set; }
        
        [XmlElement]
        public DateTime TimeStamp { get; set; }
        
        [XmlElement]
        public string FormattedMessage { get; set; }
        
        [XmlElement]
        public string Message { get; set; }
        
        [XmlElement]
        public string StackTrace { get; set; }
        
        [XmlElement]
        public string LoggerName { get; set; }
        
        [XmlElement]
        public int Progress { get; set; }
        
        [XmlElement]
        public ProgressState? ProgressState { get; set; }
        
        [XmlIgnore]
        public Exception Exception { get; set; }
        
        [XmlElement]
        public LogLevel Level { get; set; }
        
        [XmlElement]
        public string Source { get; set; }
        
        [XmlIgnore]
        public object SourceObject { get; set; }
        
        [XmlElement]
        public string ThreadName { get; set; }
        
        [XmlElement]
        public string Url { get; set; }

        public LogEventInfo() { }

        public LogEventInfo(object source, string loggerName, LogLevel level, string message, object[] args, Exception exception = null, int progress = -1, ProgressState? progressState = null)
        {
            SourceObject = source;
            Source = source.ToString();
            LoggerName = loggerName;
            Level = level;
            Message = message ?? "<null>";
            FormattedMessage = args != null ? string.Format(Message, args) : Message;
            Exception = exception;
            StackTrace = exception != null ? exception.StackTrace : string.Empty;
            Progress = progress;
            ProgressState = progressState;
            TimeStamp = DateTime.Now;
            ThreadName = Thread.CurrentThread.Name;
            Id = GetUniqueId();
        }

        private static long GetUniqueId()
        {
            lock (_locker)
            {
                return ++_id;
            }
        }
    }
}
