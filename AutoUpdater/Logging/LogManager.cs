﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System;

namespace AutoUpdater.Logging
{
    public static class LogManager
    {
        public static List<ITarget> Targets { get; private set; }

        public static Dictionary<string, ConsoleColor> Colors { get; private set; }
        public static LogLevel MinLogLevel { get; set; }

        // ctor

        #region static LogManager()
        static LogManager()
        {
            Targets = new List<ITarget>();
            Colors = new Dictionary<string, ConsoleColor>();
            MinLogLevel = LogLevel.Debug;
        }
        #endregion

        // public methods

        #region GetCurrentClassLogger()
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static Logger GetCurrentClassLogger(object loggerName = null)
        {
            StackFrame frame = new StackFrame(1, false);
            return new Logger(frame.GetMethod().DeclaringType, loggerName);
        }
        #endregion

        #region Announce(LogEventInfo item)
        public static void Announce(LogEventInfo item)
        {
            // skip possible logging level
            if (MinLogLevel.Equals(LogLevel.Off) || MinLogLevel > item.Level) return;
            
            //Console.WriteLine(string.Format("<{0}> {1}", item.Source, item.FormattedMessage));

            // simple
            if (Targets.Count == 1)
            {
                Targets[0].Announce(item);
            }
            // more targets
            else
            {
                foreach (var target in Targets)
                {
                    target.Announce(item);
                }
            }
        }
        #endregion
    }
}
