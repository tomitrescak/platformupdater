﻿using System;
using System.IO;

namespace AutoUpdater.Logging
{
    public class FileTarget : ITarget
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private object _locker = new object();
        private string _fileName;

        public FileTarget(string fileName)
        {
            try
            {
                _fileName = fileName;
                // create initial file
                if (!File.Exists(_fileName))
                {
                    using (var fs = File.Create(_fileName))
                    {
                        fs.Close();
                    }
                }
            } catch (Exception ex)
            {
                LogManager.Targets.Remove(this);
                Log.ErrorException(ex, "Error writing log file: '{0}', removing file log!", fileName);
            }
        }

        public void Announce(LogEventInfo item)
        {
            try {
                // progress is not logged to file
                if (item.Level.Equals(LogLevel.Progress)) return;
                if (string.IsNullOrEmpty(_fileName)) return;

                lock (_locker)
                {
                    using (var fs = File.AppendText(_fileName))
                    {
                        fs.WriteLine("[{0}] {1} - {2}{3}", item.TimeStamp, item.Source, item.FormattedMessage, FormatException(item.Exception));
                        fs.Flush();
                        fs.Close();
                    }
                }
            } catch (Exception ex)
            {
                LogManager.Targets.Remove(this);
                Log.ErrorException(ex, "Error writing log file '{0}', removing file log!", _fileName);
            }
        }

        private string FormatException(Exception ex)
        {
            if (ex == null) return string.Empty;
            var exd = string.Format("\n----------------------------\n{0}----------------------------\n{1}", ex.Message,
                                    ex.StackTrace);
            if (ex.InnerException != null) return FormatException(ex.InnerException) + exd;
            return exd;
        }
    }
}
