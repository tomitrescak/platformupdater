﻿using System;

namespace AutoUpdater.Logging
{
    #region LogLevel
    public enum LogLevel
    {
        Off,
        Debug,
        Info,
        Warn,
        Error,
        Fatal,
        Progress
    }
    #endregion


    public class Logger
    {
        public string Name { get; set; }
        public string Source { get; set; }
        public static string Version { get { return "1.0"; } }
         
        // ctor

        #region  Logger(Type declaringType)
        public Logger(Type declaringType, object source)
        {
            Name = declaringType.ToString();
            Source = source == null ? declaringType.Name : source.ToString();
        }
        #endregion

        // public methods

        #region Debug(string message, params object[] args)
        public void Debug(string message, params object[] args)
        {
            Log(LogLevel.Debug, message, args);
        }
        #endregion

        #region DebugException(string message, params object[] args)
        public void DebugException(Exception ex, string message, params object[] args)
        {
            Log(LogLevel.Debug, message, args, ex);
        }
        #endregion

        #region Info(string message, params object[] args)
        public void Info(string message, params object[] args)
        {
            Log(LogLevel.Info, message, args);
        }
        #endregion

        #region InfoException(string message, params object[] args)
        public void InfoException(Exception ex, string message, params object[] args)
        {
            Log(LogLevel.Info, message, args, ex);
        }
        #endregion

        #region Warn(string message, params object[] args)
        public void Warn(string message, params object[] args)
        {
            Log(LogLevel.Warn, message, args);
        }
        #endregion

        #region WarnException(string message, Exception exception)
        public void WarnException(Exception exception, string message, params object[] args)
        {
            Log(LogLevel.Warn, message, args, exception);
        }
        #endregion

        #region Error(string message, params object[] args)
        public void Error(string message, params object[] args)
        {
            Log(LogLevel.Error, message, args);
        }
        #endregion

        #region ErrorException(string message, Exception exception)
        public void ErrorException(Exception exception, string message, params object[] args)
        {
            Log(LogLevel.Error, message, args, exception);
        }
        #endregion

        #region Fatal(string message, params object[] args)
        public void Fatal(string message, params object[] args)
        {
            Log(LogLevel.Fatal, message, args);
        }
        #endregion

        #region FatalException(Exception exception, string message, params object[] args)
        public void FatalException(Exception exception, string message, params object[] args)
        {
            Log(LogLevel.Fatal, message, args, exception);
        }
        #endregion

        #region Progress(string message, params object[] args)
        public void Progress(int progress, ProgressState progressState, string message, params object[] args)
        {
            Log(LogLevel.Progress, message, args, null, progress, progressState);
        }
        #endregion

        #region Log(LogLevel level, string message, object[] args, Exception exception)
        public void Log(LogLevel level, string message, object[] args, Exception exception = null, int progress = -1, ProgressState? progressState = null)
        {
            LogManager.Announce(new LogEventInfo(Source, Name, level, message, args, exception, progress, progressState));
        }
        #endregion
    }
}
