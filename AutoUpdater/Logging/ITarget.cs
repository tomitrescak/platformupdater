﻿namespace AutoUpdater.Logging
{
    public interface ITarget
    {
        void Announce(LogEventInfo item);
    }
}
