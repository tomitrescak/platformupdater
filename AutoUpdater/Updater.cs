﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using AutoUpdater.Logging;

namespace AutoUpdater
{
    public class Updater
    {
        private Logger Log = LogManager.GetCurrentClassLogger();

        public const string UpdaterPrefix = "M1234_";
        private string _app;

        public static List<VersionInfo> Updates { get; set; }

        private readonly string _downloadsurl;
        private readonly string _appDirectory;
        private readonly Version _currentVersion;
        private readonly DateTime _currentDate;

        public Updater(Assembly ass, string downloadsurl)
            : this(ass.Location, ass, downloadsurl)
        {
        }

        public Updater(string appPath, Assembly ass, string downloadsurl)
        {

            _downloadsurl = downloadsurl;
            _app = appPath;
            _currentVersion = ass.GetName().Version;
            _currentDate = File.GetLastWriteTime(appPath);
            _appDirectory = Path.GetDirectoryName(_app);
            
            // update updater
            
            // UpdateHandler.UpdateMe(UpdaterPrefix, _appDirectory);
        }

        #region Update(bool delete = true)
        public void Update(bool delete = true, bool useGui = true, bool kill = true, bool launch = true)
        {
            if (Updates.Count > 0)
            {
                // proceed all updates
                UpdateHandler.InstallUpdateRestart(Updates, _app, delete, useGui, kill, launch);
            }
        } 
        #endregion

        #region CheckUpdate()
        public List<VersionInfo> CheckUpdate()
        {
            try
            {
                var possibleUpdates = UpdateHandler.GetUpdateInfo(_downloadsurl);

                if (possibleUpdates == null)
                {
                    Log.Warn("Error retreiving update info.");
                    return null;
                }

                Updates = new List<VersionInfo>();
                foreach (var possibleUpdate in possibleUpdates)
                {
                    var sdate = DateTime.ParseExact(possibleUpdate.Date, "dd/MM/yyyy HH:mm:ss z",
                                                    CultureInfo.InvariantCulture);
                    // Log.Info("Local file: '{0}' vs. server file: '{1}'", _currentDate, sdate);

                    if (_currentVersion.CompareTo(new Version(possibleUpdate.Version)) == -1 ||
                        (_currentVersion.CompareTo(new Version(possibleUpdate.Version)) == 0 &&
                         _currentDate.CompareTo(sdate) == -1))
                    {
                        Updates.Add(possibleUpdate);
                        Log.Info("Update {0} for '{1}' created '{2}' is available!", possibleUpdate.Version, _app, possibleUpdate.Date);
                    }
                    else
                    {
                        break;
                    }
                }

                return Updates;
            }
            catch (Exception ex)
            {
                Log.Error("Error updating app: " + ex.Message);
            }
			return null;
        } 
        #endregion
    }
}
