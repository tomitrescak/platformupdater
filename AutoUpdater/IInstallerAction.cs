﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AutoUpdater
{
    public interface IInstallerAction
    {
        string[] PreInstallCommands { get; }
        bool PreExecute();
        string[] PostInstallCommands { get; }
        bool PostExecute();

    }
}
