﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace AutoUpdater
{
    [XmlRoot]
    public class VersionInfo
    {
        [XmlElement]
        public string Version { get; set; }
        [XmlElement]
        public string Url { get; set; }
        [XmlElement]
        public string Description { get; set; }
        [XmlElement]
        public string Date { get; set; }
        [XmlElement]
        public List<string> PreInstallActions { get; set; }
        [XmlElement]
        public List<string> PostInstallActions { get; set; }

        public VersionInfo() { }

        public VersionInfo(XmlNode node)
        {
            Date = node.SelectSingleNode("Date").InnerText;
            Version = node.SelectSingleNode("Version").InnerText;
            Url = node.SelectSingleNode("Url").InnerText;
            Description = node.SelectSingleNode("Description").InnerText;
        }
    }
}
