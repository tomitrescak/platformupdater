﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Serialization;
using AutoUpdater.Logging;
using Ionic.Zip;
using System.Reflection;
//using SharpCompress.Archive.Zip;
//using SharpCompress.Archive;
//using SharpCompress.Common;

namespace AutoUpdater
{
    public class UpdateProcess
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        bool called = true;

        private IUpdaterGui _updaterGui;

        private string _tempDownloadFolder;
        private string _appPath = "";
        private string _updaterFile = "";
        private string _appFolder = "";
        private string _appName = "";
        private string _downloadFile = "";
        private List<string> _versions;
        private List<VersionInfo> _versionsInfo;
        private bool _deleteUpdateFiles;
        private bool _kill;
        private bool _launch;

        private const string UpdateFolder = "Updates";
        private string _zipFile;


        // ctor

        public UpdateProcess(IUpdaterGui gui)
        {
            LogManager.Targets.Add(new FileTarget("updater.log"));
            _updaterGui = gui;
        }

        #region Start()
        public void Start()
        {
            var bw = new BackgroundWorker();
            bw.DoWork += BackgroundWorker;
            bw.WorkerSupportsCancellation = true;
            bw.RunWorkerAsync();
        }
        #endregion

        #region SetLabel(Label label, string text)
        public void SetLabel(string text)
        {
            _updaterGui.ShowMessage(text);
        }
        #endregion

        #region BackgroundWorker(object sender, DoWorkEventArgs e)
        private void BackgroundWorker(object sender, DoWorkEventArgs e)
        {
            UnpackCommandline();

            if (called)
            {
                // load version files
                _versionsInfo = LoadVersions();
                if (_versionsInfo == null)
                {
                    return;
                }

                try
                {
                    // if we are killing mono processes make sure user wants that!

                    if (_kill)
                    {
                        if (_appName == "mono")
                        {
                            if (!_updaterGui.Ask("All 'mono' processes will be closed, do you want to continue?"))
                            {
                                _updaterGui.Quit();
                                return;
                            }
                        }

                    
                        Log.Info("Stopping: " + _appName);
                        SetLabel("Stopping " + _appName);
                        Thread.Sleep(1000);

                        var processes = Process.GetProcessesByName(Path.GetFileNameWithoutExtension(_appName));
                        var pid = Process.GetCurrentProcess().Id;

                        foreach (var process in processes)
                        {
                            try
                            {
                                if (process.Id != pid)
                                {
                                    Log.Info("Killing: " + _appName);
                                    process.Kill();
                                }
                            }
                            catch
                            {
                            }
                        }
                    }

                    Webdata.BytesDownloaded += Bytesdownloaded;
                    foreach (var version in _versions)
                    {
                        var versionInfo = _versionsInfo.Find(w => w.Version == version);
                        if (versionInfo == null)
                        {
                            Log.Error("Version {0} does not exist!", version);
                            continue;
                        }

                        PreDownload(versionInfo.Url);
                        if (!Webdata.DownloadFromWeb(versionInfo.Url, _tempDownloadFolder))
                        {
                            Log.Error("Unable to download file from: " + versionInfo.Url);
                            WrapUp(_deleteUpdateFiles);
                            _updaterGui.Quit();
                            return;
                        }

                        SetLabel("Unzipping package " + Path.GetFileName(versionInfo.Url) + " ...");
                        Thread.Sleep(1000);
                        UnZip(Path.Combine(_tempDownloadFolder, _downloadFile), _tempDownloadFolder);

                        SetLabel("Executing pre-install actions");
                        // get all installer actions
                        var actionsFactory = new InstallerActions(_tempDownloadFolder);
                        if (!actionsFactory.ExectutePreCommands(versionInfo) || !actionsFactory.ExectutePreActions())
                            continue;

                        SetLabel("Moving files ...");
                        Thread.Sleep(1000);
                        MoveFiles();
                        SetLabel("Wrapping up ...");
                        Thread.Sleep(1000);
                        WrapUp(_deleteUpdateFiles);

                        actionsFactory.ExectutePostCommands(versionInfo);
                        actionsFactory.ExectutePostActions();
                    }

                    PostDownload();
                }
                catch (Exception ex)
                {
                    SetLabel(ex.Message);
                    Thread.Sleep(2000);
                    Log.ErrorException(ex, "Could not stop the process: " + ex.Message + ex.StackTrace);
                    return;
                }

            }
            _updaterGui.Quit();
        }
        #endregion

        #region UnpackCommandline()
        private void UnpackCommandline()
        {
            try
            {
                _versions = new List<string>();

                var args = Environment.GetCommandLineArgs();
                Log.Info("Started update with params '{0}'", string.Join(" ", args));
                for (var i = 0; i < args.Length; i++)
                {
                    if (args[i] == "--version")
                    {
                        _versions.Add(args[++i]);
                    }
                    else if (args[i] == "--app")
                    {
                        _appPath = args[++i];
                        _appFolder = Path.GetDirectoryName(_appPath);
                    }
                    else if (args[i] == "--process")
                    {
                        _appName = args[++i];
                    }
                    else if (args[i] == "--delete")
                    {
                        _deleteUpdateFiles = bool.Parse(args[++i]);
                    }
                    else if (args[i] == "--kill")
                    {
                        _kill = bool.Parse(args[++i]);
                    }
                    else if (args[i] == "--launch")
                    {
                        _launch = bool.Parse(args[++i]);
                    }
                    else if (args[i] == "--updaterFile")
                    {
                        _updaterFile = args[++i];
                    }

                }
            }
            catch (Exception ex)
            {
                string ee = ex.Message;
            }
        }
        #endregion

        #region UnZip(string file, string unZipTo)
        private void UnZip(string file, string unZipTo)
        {
            try
            {
                _zipFile = Path.GetFileName(file);
                // Specifying Console.Out here causes diagnostic msgs to be sent to the Console
                // In a WinForms or WPF or Web app, you could specify nothing, or an alternate
                // TextWriter to capture diagnostic messages. 

                Log.Info("Unzipping '{0}' to '{1}'", file, unZipTo);

                using (ZipFile zip = ZipFile.Read(file))
                {
                    // This call to ExtractAll() assumes:
                    //   - none of the entries are password-protected.
                    //   - want to extract all entries to current working directory
                    //   - none of the files in the zip already exist in the directory;
                    //     if they do, the method will throw.
                    zip.ExtractAll(unZipTo);
                }

                // SharpZlib way
                //				using (var reader = ZipArchive.Open(file))
                //				{
                //					foreach (var entry in reader.Entries)
                //					{
                //						entry.WriteToDirectory(unZipTo, ExtractOptions.ExtractFullPath | ExtractOptions.Overwrite);
                //					}
                //				}
            }
            catch (System.Exception ex)
            {
                Log.ErrorException(ex, "Error unzipping file!:" + file);
            }
        }
        #endregion

        #region PreDownload(string url)
        private void PreDownload(string url)
        {

            var dirName = Path.GetFileNameWithoutExtension(url);
            if (!Directory.Exists(UpdateFolder)) Directory.CreateDirectory(UpdateFolder);

            _downloadFile = Path.GetFileName(url);
            _tempDownloadFolder = Path.Combine(Path.GetDirectoryName(_appPath), UpdateFolder, dirName);

            if (Directory.Exists(_tempDownloadFolder))
            {
                Directory.Delete(_tempDownloadFolder, true);
            }

            Directory.CreateDirectory(_tempDownloadFolder);
        }
        #endregion

        #region PostDownload()
        private void PostDownload()
        {
            if (_launch)
            {
                var path = Path.Combine(_appFolder, _appPath);
                Log.Info("Launching: " + path);
                var startInfo = new ProcessStartInfo {FileName = path};
                //startInfo.Arguments = postProcessCommand;
                Process.Start(startInfo);
            }

        }
        #endregion

        #region WrapUp(bool delete)
        private void WrapUp(bool delete)
        {
            if (delete && Directory.Exists(_tempDownloadFolder))
            {
                Log.Info("Deleting: " + _tempDownloadFolder);
                Directory.Delete(_tempDownloadFolder, true);
            }

        }
        #endregion

        #region MoveFiles()
        private void MoveFiles()
        {

            var di = new DirectoryInfo(_tempDownloadFolder);
            Log.Info("Moving '{0}' files from '{1}'", di.GetFiles().Length, _tempDownloadFolder);

            try
            {
                MoveFilesFromDirectory(di, string.Empty);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                _updaterGui.ShowDialog("Update error!: " + ex.Message);
            }
            Log.Info("Moving finished");
        }
        #endregion

        private void MoveFilesFromDirectory(DirectoryInfo di, string directoryName)
        {
            DirectoryInfo[] directories = di.GetDirectories();
            

            foreach (var directory in directories)
            {
                var dir = Path.Combine(_appFolder, directory.Name);
                if (!Directory.Exists(dir))
                {
                    Log.Info("Creating directory '{0}'", directory.Name);
                    Directory.CreateDirectory(dir);
                }

                // move all files
                MoveFilesFromDirectory(directory, Path.Combine(directoryName, directory.Name));
            }

            FileInfo[] files = di.GetFiles();
            MoveFiles(files, directoryName);
        }

        private void MoveFiles(FileInfo[] files, string directory)
        {
            foreach (FileInfo fi in files)
            {
                // installer files are skipped
                if (fi.Name.Equals(_zipFile)) continue;

                SetLabel("Moving " + fi.Name);
                Log.Info("Moving from '{0}' to '{1}' ", Path.Combine(_tempDownloadFolder, directory, fi.Name), Path.Combine(_appFolder, directory, fi.Name));
                if (fi.Name != _downloadFile)
                {
                    File.Copy(Path.Combine(_tempDownloadFolder, directory, fi.Name), Path.Combine(_appFolder, directory, fi.Name), true);
                }
            }
        }

        #region Bytesdownloaded(ByteArgs e)
        private void Bytesdownloaded(ByteArgs e)
        {
            SetLabel(string.Format("Downloading Update [{0}/{1} KB]", e.Downloaded / 1024, e.Total / 1024));

            _updaterGui.ShowProgress(e.Downloaded, e.Total);
        }
        #endregion

        #region LoadVersions()
        private List<VersionInfo> LoadVersions()
        {
            try
            {
                var path = Path.Combine(Path.GetDirectoryName(_appPath), UpdateFolder, _updaterFile);
                if (!File.Exists(path))
                {
                    return null;
                }

                List<VersionInfo> versions;
                using (var fs = new FileStream(path, FileMode.Open))
                {
                    var sr = new XmlSerializer(typeof(List<VersionInfo>));
                    versions = (List<VersionInfo>)sr.Deserialize(fs);
                    fs.Close();
                }
                return versions;
            }
            catch (Exception ex)
            {
                Log.ErrorException(ex, "Error loading version file: " + ex.Message);
                return null;
            }
        }
        #endregion
    }
}
