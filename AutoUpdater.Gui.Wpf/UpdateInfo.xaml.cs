﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AutoUpdater.Gui.Wpf
{
    /// <summary>
    /// Interaction logic for UpdateInfo.xaml
    /// </summary>
    public partial class UpdateInfo : Window
    {
        private Updater _updater;

        public UpdateInfo()
        {
            InitializeComponent();
        }

        public UpdateInfo(Updater updater, Assembly ass, List<AutoUpdater.VersionInfo> versions)
            : this()
        {
            _updater = updater;

            var sb = new StringBuilder();
            foreach (var version in versions)
            {
                sb.Append("<style type=\"text/css\">body {font-size:10px;font-family:verdana }</style>");
                sb.Append("<span style=\"font-size: 11px\"><b>Version:</b> " + version.Version + "</span><br /><hr />");
                sb.Append("<i>Date:</i> " + version.Date + "</br>");
                sb.Append("<ul>");
                foreach (var info in version.Description.Split('\n'))
                {
                    sb.Append("<li>" + System.Net.WebUtility.HtmlEncode(info) + "</li>");
                }
                sb.Append(version.Description + "</ul></br>");

            }
            HtmlViewer.NavigateToString(sb.ToString());
            YourVersion.Content = ass.GetName().Version.ToString();
            NewVersion.Content = versions[0].Version;

        }

        private void Cancel(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void Update(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            _updater.Update(false, true);
            Close();
        }
    }
}
