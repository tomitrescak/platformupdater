﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AutoUpdater.Gui.Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IUpdaterGui
    {
        public MainWindow()
        {
            InitializeComponent();

            var updater = new UpdateProcess(this);
            updater.Start();
        }

        #region IUpdaterGui implementation

        #region ShowDialog(string message)
        public void ShowDialog(string message)
        {
            Dispatcher.Invoke(new Action(() => MessageBox.Show(message, "Updater", MessageBoxButton.OK, MessageBoxImage.Information)));
        }
        #endregion

        #region ShowMessage(string message)
        public void ShowMessage(string message)
        {
            Dispatcher.Invoke(new Action(() => Status.Content = message));
        }
        #endregion

        #region ShowProgress(int progress, int total)
        public void ShowProgress(int progress, int total)
        {
            Dispatcher.Invoke(new Action(() => Progress.Value = (int)(100 * ((double)progress / (double)total))));
        }
        #endregion

        #region Quit()
        public void Quit()
        {
            System.Environment.Exit(0);
            Application.Current.Shutdown();
        }
        #endregion

        #region Ask(string question)
        public bool Ask(string question)
        {
            bool result = false;
            var waiter = new AutoResetEvent(false);
            Dispatcher.Invoke(new Action(() =>
            {
                if (MessageBox.Show(question, "Updater", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    result = true;
                }
                waiter.Set();
            }));
            waiter.WaitOne();
            return result;
        }
        #endregion

        #endregion    
    }
}
