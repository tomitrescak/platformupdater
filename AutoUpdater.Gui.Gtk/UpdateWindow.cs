﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using AutoUpdater.Logging;
//using SharpCompress.Archive.Zip;
//using SharpCompress.Archive;
//using SharpCompress.Common;
using Gtk;

namespace AutoUpdater.Gtk
{
    public class UpdateWindow : Window, IUpdaterGui
    {     
        // ui

        private Label _statusLabel;
        private ProgressBar _statusProgress;

        // ctor
		 
		public UpdateWindow() : base("Platform updater > Making things better ...")
        {
            LogManager.Targets.Add(new FileTarget("updater.log"));

            //Width = 500;
            //Height = 60;

            _statusLabel = new Label();
            _statusProgress = new ProgressBar();

            var vBox = new VBox();
            vBox.PackStart(_statusLabel);
            vBox.PackStart(_statusProgress, true, true, 3);

            Add(vBox);
		
			var updater = new UpdateProcess(this);
			updater.Start();
        }

		#region IUpdaterGui implementation

		#region ShowDialog(string message)
		public void ShowDialog(string message)
		{

			Application.Invoke (delegate {
				MessageDialog md = new MessageDialog (this, 
				                                      DialogFlags.DestroyWithParent, MessageType.Warning, 
				                                      ButtonsType.Ok, message);
				md.Run ();
				md.Destroy ();
			});
		}
		#endregion

		#region ShowMessage(string message)
		public void ShowMessage(string message)
		{
			Application.Invoke (delegate { _statusLabel.Text = message; });	
		}
		#endregion

		#region ShowProgress(int progress, int total)
		public void ShowProgress(int progress, int total)
		{
			Application.Invoke (delegate { _statusProgress.Fraction = (double)progress / (double)total; });	
		}
		#endregion

		#region Quit()
		public void Quit()
		{
			Application.Quit ();
		}
		#endregion

		#region Ask(string question)
		public bool Ask(string question)
		{
			bool result = false;
			var waiter = new AutoResetEvent (false);
			Application.Invoke (delegate {
				MessageDialog md = new MessageDialog (this, 
				                                      DialogFlags.DestroyWithParent, MessageType.Question, 
				                                      ButtonsType.YesNo, question);
				if (md.Run () == (int)ResponseType.Yes) {
					result = true;
				} 
				md.Destroy ();
				waiter.Set ();
			});
			waiter.WaitOne ();
			return result;
		}
		#endregion

		#endregion
    }
}
