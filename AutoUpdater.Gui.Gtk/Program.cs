using System;
using Gtk;
using System.Reflection;

namespace AutoUpdater.Gtk
{
	class MainClass
	{
		[STAThread]
		static void Main()
		{
			Application.Init();
			var myWin = new UpdateWindow();
			myWin.Resize(300, 80);
			myWin.Destroyed += new EventHandler(myWin_Destroyed);   
			myWin.Resizable = false;
			myWin.ShowAll();

			Application.Run();
		}
		
		static void myWin_Destroyed(object sender, EventArgs e)
		{
			Application.Quit();
		}
	}
}
