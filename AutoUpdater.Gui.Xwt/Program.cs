using System;
using Xwt;

namespace AutoUpdater.Gui.Xwt
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Application.Initialize(ToolkitType.Gtk); // Set correct type 
			
			UpdateWindow w = new UpdateWindow();
			w.Show();
			
			Application.Run();
			
			w.Dispose();
		}
	}
}
