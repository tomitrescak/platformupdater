using System;
using Xwt;
using System.Collections.Generic;
using System.Text;
using AutoUpdater;

namespace Livebox.AutoUpdater
{
	public class UpdatePreview : Window
	{
		private VBox mainBox = new VBox();
		private HBox splitbox = new HBox();
		private HBox buttonbox = new HBox();
		private Label warningLabel = new Label() { Wrap = WrapMode.Word, MinWidth = 150 };
		private RichTextView textView = new RichTextView();
		private Label confirm = new Label("Do you wish to proceed?");
		private Button confirmButton = new Button("Yes");
		private Button cancelButton = new Button("No");

		public UpdatePreview (Updater updater, List<VersionInfo> versions)
		{
			Title = "New updates are available!";
			Resizable = false;

			Width = 250;
			Height = 200;

			warningLabel.Margin = 6;
			warningLabel.Text = "Your application is outdated. We have a fresh new version for you! Please back up your application before proceeding. We do not take any responsability for lost data or application malfunction after update.";

			var sb = new StringBuilder();
			foreach (var version in versions) {
				sb.Append("------------------------------------------------\n");
				sb.Append("Version: " + version.Version + " (" + version.Date + ")\n");
				sb.Append("------------------------------------------------\n");
				sb.Append(version.Description + "\n\n");
			}

			confirmButton.Clicked += (sender, e) => {
				updater.Update();
			};

			cancelButton.Clicked += (sender, e) => {
				Dispose();
			};

			textView.LoadText(sb.ToString(), Xwt.Formats.TextFormat.Plain);

			splitbox.PackStart(warningLabel);
			splitbox.PackStart(textView);

			buttonbox.PackStart(confirm);
			buttonbox.PackEnd(confirmButton);
			buttonbox.PackEnd(cancelButton);

			mainBox.PackStart(splitbox);
			mainBox.PackStart(buttonbox);
			Content = mainBox;
		}
	}
}

