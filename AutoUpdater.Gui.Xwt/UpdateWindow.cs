using System;
using Xwt;
using System.Threading;

namespace AutoUpdater.Gui.Xwt
{
	public class UpdateWindow : Window, IUpdaterGui
	{
		private Label _statusLabel;
		private ProgressBar _statusProgress;


		public UpdateWindow()
		{			
			Title = "Platform updater > Making things better ...";
			Width = 500;
			Height = 60;
			
			_statusLabel = new Label();
			_statusProgress = new ProgressBar();
			
			var vBox = new VBox();
			vBox.PackStart(_statusLabel);
			vBox.PackStart(_statusProgress, BoxMode.FillAndExpand);
			
			Content = vBox;

			
			var updater = new UpdateProcess(this);
			updater.Start();
		}
		
		#region IUpdaterGui implementation
		
		#region ShowDialog(string message)
		public void ShowDialog(string message)
		{
			Application.Invoke(() => MessageDialog.ShowMessage(message));
		}
		#endregion
		
		#region ShowMessage(string message)
		public void ShowMessage(string message)
		{
			Application.Invoke(() =>  _statusLabel.Text = message);
		}
		#endregion
		
		#region ShowProgress(int progress, int total)
		public void ShowProgress(int progress, int total)
		{
			Application.Invoke(() => _statusProgress.Fraction = (double) progress/(double) total);
		}
		#endregion
		
		#region Quit()
		public void Quit()
		{
			Application.Exit();
		}
		#endregion
		
		#region Ask(string question)
		public bool Ask(string question)
		{
			bool result = false;
			var waiter = new AutoResetEvent(false);
			Application.Invoke(() => {
				if (!MessageDialog.Confirm(question, Command.Ok)) {
					result = true;
				}
				waiter.Set();
			});
			waiter.WaitOne();
			return result;
		}
		#endregion
		
		#endregion   
	}
}

