﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using AutoUpdater;

namespace UpdateTester
{
    [Export(typeof(IInstallerAction))]
    public class InstallerAction : IInstallerAction
    {
        private static readonly string[] Pre = { "cmd.exe /c echo wahoooo!", "cmd.exe /c echo wahoooo2!" };
        private static readonly string[] Post = { "cmd.exe /c echo rrrr!", "cmd.exe /c echo ssss" };

        #region PreInstallCommands
        public string[] PreInstallCommands
        {
            get { return Pre; }
        } 
        #endregion

        #region PreExecute()
        public bool PreExecute()
        {
            return true;
        } 
        #endregion

        #region PostInstallCommands
        public string[] PostInstallCommands
        {
            get { return Post; }
        } 
        #endregion

        #region PostExecute()
        public bool PostExecute()
        {
            return true;
        } 
        #endregion

    }
}
