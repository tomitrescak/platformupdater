﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace AutoUpdater.Gui.WinForms
{
    public partial class UpdateWindow : Form, IUpdaterGui
    {
        public UpdateWindow()
        {
            InitializeComponent();

            var updater = new UpdateProcess(this);
            updater.Start();
        }

        #region IUpdaterGui implementation

        #region ShowDialog(string message)
        public void ShowDialog(string message)
        {
            Invoke(new Action(() => MessageBox.Show(message, "Updater", MessageBoxButtons.OK, MessageBoxIcon.Information)));
        }
        #endregion

        #region ShowMessage(string message)
        public void ShowMessage(string message)
        {
            Invoke(new Action(() => label1.Text = message));
        }
        #endregion

        #region ShowProgress(int progress, int total)
        public void ShowProgress(int progress, int total)
        {
            Invoke(new Action(() => progressBar1.Value = (int) (100*((double) progress/(double) total))));
        }
        #endregion

        #region Quit()
        public void Quit()
        {
            Application.Exit();
        }
        #endregion

        #region Ask(string question)
        public bool Ask(string question)
        {
            bool result = false;
            var waiter = new AutoResetEvent(false);
            Invoke(new Action(() => {
                if (MessageBox.Show(question, "Updater", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    result = true;
                }
                waiter.Set();
            }));
            waiter.WaitOne();
            return result;
        }
        #endregion

        #endregion    
    }
}
