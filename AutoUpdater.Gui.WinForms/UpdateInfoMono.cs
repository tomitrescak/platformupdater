﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace AutoUpdater.Gui.WinForms
{
    public partial class UpdateInfoMono : Form
    {
        public UpdateInfoMono()
        {
            InitializeComponent();

            this.FormBorderStyle = FormBorderStyle.FixedSingle;
        }

        public UpdateInfoMono(Updater updater, Assembly ass, List<AutoUpdater.VersionInfo> versions)
            : this()
        {
            foreach (var version in versions)
            {
                versionInfoView.AppendText("Version: " + version.Version + "\n\n");
                //versionInfoView.AppendText("Date: " + version.Date + "\n\n");

                foreach (var info in version.Description.Split('\n'))
                {
                    versionInfoView.AppendText("- " + info);
                }

            }
            currentVersion.Text = ass.GetName().Version.ToString();
            newVersion.Text = versions[0].Version;

            updateButton.Click += (sender, e) =>
                                      {
                                          DialogResult = DialogResult.Yes;
                                          updater.Update(false);
                                          Close();
                                      };

            cancelButton.Click += (sender, e) =>
            {
                DialogResult = DialogResult.No;
                Close();
            };
        }
    }
}
