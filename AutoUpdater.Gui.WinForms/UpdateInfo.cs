﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace AutoUpdater.Gui.WinForms
{
    public partial class UpdateInfo : Form
    {
        public UpdateInfo()
        {
            InitializeComponent();
        }

        public UpdateInfo(Updater updater, Assembly ass, List<AutoUpdater.VersionInfo> versions)
            : this()
        {
            var sb = new StringBuilder();
            foreach (var version in versions)
            {
                sb.Append("<style type=\"text/css\">body {font-size:10px;font-family:verdana }</style>");
                sb.Append("<span style=\"font-size: 11px\"><b>Version:</b> " + version.Version + "</span><br /><hr />");
                sb.Append("<i>Date:</i> " + version.Date + "</br>");
                sb.Append("<ul>");
                foreach (var info in version.Description.Split('\n'))
                {
                    sb.Append("<li>" + info + "</li>");
                }
                sb.Append(version.Description + "</ul></br>");

            }
            versionView.DocumentText = sb.ToString();
            currentVersion.Text = ass.GetName().Version.ToString();
            newVersion.Text = versions[0].Version;

            updateButton.Click += (sender, e) =>
                                      {
                                          DialogResult = DialogResult.Yes;
                                          updater.Update();
                                          Close();
                                      };

            cancelButton.Click += (sender, e) => {
                DialogResult = DialogResult.No;
                                                     Close();
            };
        }
    }
}
